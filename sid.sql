-- ----------------------------
-- Function structure for f_cluster_bmk
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cluster_bmk`;
delimiter ;
CREATE FUNCTION `f_cluster_bmk`(vnilai varchar(200))
 RETURNS varchar(50) CHARSET latin1
BEGIN
    case 
        when vnilai< 2500000000 then return 'K';
				when vnilai>= 2500000000 and vnilai<50000000000 then return 'M';
        when vnilai>= 50000000000 and vnilai<100000000000 then return 'B';
        when vnilai>= 100000000000 then return 'B';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_cluster_kategori
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cluster_kategori`;
delimiter ;;
CREATE FUNCTION `f_cluster_kategori`(vnilai varchar(50))
 RETURNS varchar(50) CHARSET latin1
BEGIN
    case 
        when vnilai< 2500000000 then return '0<2,5M';
				when vnilai>= 2500000000 and vnilai<50000000000 then return '2,5<50M';
        when vnilai>= 50000000000 and vnilai<100000000000 then return '50<100M';
        when vnilai>= 100000000000 then return '100M';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_cluster_paket
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cluster_paket`;
delimiter ;;
CREATE FUNCTION `f_cluster_paket`(vnilai varchar(50))
 RETURNS varchar(50) CHARSET latin1
BEGIN
    case 
        when vnilai< 750000000 then return '0<750JT';
        when vnilai>= 750000000 and vnilai<2500000000 then return '750JT<2,5M';
				when vnilai>= 2500000000 and vnilai<50000000000 then return '2,5<50M';
        when vnilai>= 50000000000 and vnilai<100000000000 then return '50<100M';
        when vnilai>= 100000000000 then return '100M';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_cluster_paket_konsultan
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cluster_paket_konsultan`;
delimiter ;;
CREATE FUNCTION `f_cluster_paket_konsultan`(vnilai varchar(200))
 RETURNS varchar(150) CHARSET latin1
BEGIN
    case 
        when vnilai< 750000000 then return '0<750JT';
				when vnilai>= 750000000 and vnilai<10000000000 then return '750JT<10M';
        when vnilai>= 10000000000 then return '>10M';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_cluster_paket_konsultan_n
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cluster_paket_konsultan_n`;
delimiter ;;
CREATE FUNCTION `f_cluster_paket_konsultan_n`(vnilai varchar(50))
 RETURNS varchar(50) CHARSET latin1
BEGIN
    case 
        when vnilai<= 1000000000 then return '0<1M';
				when vnilai> 1000000000 and vnilai<= 2500000000 then return '1<2,5M';
        when vnilai> 2500000000 then return '>2,5M';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_cluster_paket_kontraktor
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cluster_paket_kontraktor`;
delimiter ;;
CREATE FUNCTION `f_cluster_paket_kontraktor`(vnilai varchar(200))
 RETURNS varchar(150) CHARSET latin1
BEGIN
    case 
        when vnilai< 2500000000 then return '0<2,5M';
				when vnilai>= 2500000000 and vnilai<50000000000 then return '2,5<50M';
        when vnilai>= 50000000000 and vnilai<100000000000 then return '50<100M';
        when vnilai>= 100000000000 then return '100M';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_cluster_paket_kontraktor_copy1
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cluster_paket_kontraktor_copy1`;
delimiter ;;
CREATE FUNCTION `f_cluster_paket_kontraktor_copy1`(vnilai varchar(50))
 RETURNS varchar(50) CHARSET latin1
BEGIN
    case 
        when vnilai< 2500000000 then return '0<2,5M';
				when vnilai>= 2500000000 and vnilai<50000000000 then return '2,5<50M';
        when vnilai>= 50000000000 and vnilai<100000000000 then return '50<100M';
        when vnilai>= 100000000000 then return '100M';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_cluster_paket_kontraktor_n
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cluster_paket_kontraktor_n`;
delimiter ;;
CREATE FUNCTION `f_cluster_paket_kontraktor_n`(vnilai varchar(50))
 RETURNS varchar(50) CHARSET latin1
BEGIN
    case 
        when vnilai<= 10000000000 then return '0<10M';
        when vnilai> 10000000000 and vnilai<= 100000000000 then return '10<100M';
        when vnilai> 100000000000 then return '100M';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_bu
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_bu`;
delimiter ;;
CREATE FUNCTION `f_indikasi_bu`(vnilai VARCHAR ( 200 ))
 RETURNS varchar(50) CHARSET latin1
BEGIN
	CASE
			
			WHEN vnilai LIKE '01.000%' THEN
			RETURN 'BUMN';
		
		WHEN vnilai LIKE '01.001%' THEN
		RETURN 'BUMN';
		
		WHEN vnilai LIKE '01.060%' THEN
		RETURN 'BUMN';
		
		WHEN vnilai LIKE '01.061%' THEN
		RETURN 'BUMN';
		
		WHEN vnilai LIKE '31.631%' THEN
		RETURN 'BUMN';
		
		WHEN vnilai LIKE '02.058%' THEN
		RETURN 'BUMN';
		
		WHEN vnilai LIKE '73.354%' THEN
		RETURN 'BUMN';
		
		WHEN vnilai LIKE '02.058%' THEN
		RETURN 'BUMN';
		
		WHEN vnilai LIKE '11%' THEN
		RETURN 'BUMN';
		
		WHEN vnilai LIKE '76%' THEN
		RETURN 'BUMN';
		ELSE RETURN '-';
		
	END CASE;

END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_bu1
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_bu1`;
delimiter ;;
CREATE FUNCTION `f_indikasi_bu1`(vnilai varchar(200))
 RETURNS varchar(150) CHARSET latin1
BEGIN
    case 
        when vnilai LIKE '%persero%' then return 'BUMN';
				when vnilai LIKE '%telekomunikasi%' then return 'BUMN';
				when vnilai LIKE '%surveyor%' then return 'BUMN';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_bu2
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_bu2`;
delimiter ;;
CREATE FUNCTION `f_indikasi_bu2`(vnilai varchar(200))
 RETURNS varchar(150) CHARSET latin1
BEGIN
    case 
        when vnilai LIKE '%rya (persero%' then return 'BUMN';
				when vnilai LIKE '%sia (persero%' then return 'BUMN';
				when vnilai LIKE '%telekomunikasi%' then return 'BUMN';
				when vnilai LIKE '%surveyor%' then return 'BUMN';
				when vnilai LIKE '%PT. SUCOFINDO%' then return 'BUMN';
				when vnilai LIKE '%NINDYA -%' then return 'BUMN';
				when vnilai LIKE '%nindya karya' then return 'BUMN';
				when vnilai LIKE '%nindya karya wilayah%' then return 'BUMN';
				when vnilai LIKE '%PT NINDYA KARYA (Persero) WILAYAH I%' then return 'BUMN';
				when vnilai LIKE '%erumaha%' then return 'BUMN';
				when vnilai LIKE '%T. waskita%' then return 'BUMN';
				when vnilai LIKE '%WASKITA (KS%' then return 'BUMN';
				when vnilai LIKE 'WASKITA - A%' then return 'BUMN';
				when vnilai LIKE '%IKA-BRANTAS J%' then return 'BUMN';
				when vnilai LIKE '%T. PP(Persero) (K%' then return 'BUMN';
				when vnilai LIKE '%yody%' then return 'BUMN';
				when vnilai LIKE '%jo PT. YODYA KARYA (PERSERO)%' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA' then return 'BUMN';
				when vnilai LIKE '%odya%' then return 'BUMN';
				when vnilai LIKE '%brantas ab%' then return 'BUMN';
				when vnilai LIKE '%amarta karya%' then return 'BUMN';
				when vnilai LIKE '%abipraya%' then return 'BUMN';
				when vnilai LIKE '%PT Wijaya Karya' then return 'BUMN';
				when vnilai LIKE '%PT.Wijaya Karya' then return 'BUMN';
				when vnilai LIKE '%PT. Wijaya Karya' then return 'BUMN';
				when vnilai LIKE 'WIKA -%' then return 'BUMN';
				when vnilai LIKE '%PP-HK%' then return 'BUMN';
				when vnilai LIKE '%PT. WIJAYA KARYA ( Pesero )%' then return 'BUMN';
				when vnilai LIKE '%PT. WIJAYA KARYA -%' then return 'BUMN';
				when vnilai LIKE '%PP-%' then return 'BUMN';
				when vnilai LIKE '%PT. Nindya Karya%' then return 'BUMN';
				when vnilai LIKE 'PT. ADHI KARYA%' then return 'BUMN';
				when vnilai LIKE 'Adhi-Nindy%' then return 'BUMN';
				when vnilai LIKE 'PT.ADHI KARYA%' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA (Persero) Tbk' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA ' then return 'BUMN';
				when vnilai LIKE '%PT Wijaya Karya (Persero)%' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA (Persero)%' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA ( Pesero )%' then return 'BUMN';
				when vnilai LIKE 'PT. Wijaya Karya Industri %' then return 'BUMN';
				when vnilai LIKE 'PT. Wijjaya Karya (Perse%' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA BANGU%' then return 'BUMN';
				when vnilai LIKE '%PT. WIJAYA KARYA (Pers%' then return 'BUMN';
				when vnilai LIKE '%IKA-BRANTAS JO%' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA -%' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA  ' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA,%' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA ( Persero ) Tb%' then return 'BUMN';
				when vnilai LIKE '%PT. WIJAYA KARYA' then return 'BUMN';
				when vnilai LIKE 'WIKA%' then return 'BUMN';
				when vnilai LIKE 'PP-HUTAMA%' then return 'BUMN';
				when vnilai LIKE 'WIKA ?%' then return 'BUMN';
				when vnilai LIKE 'HUTAMA-%' then return 'BUMN';
				when vnilai LIKE '%(JO) - PT. HUTAMA KARYA%' then return 'BUMN';
				when vnilai LIKE 'PT.Hutama Karya%' then return 'BUMN';
				when vnilai LIKE 'PT. Hutama Karya%' then return 'BUMN';
				when vnilai LIKE '%indra karya wilayah%' then return 'BUMN';
				when vnilai LIKE 'PT. Indah KaryaJO' then return 'BUMN';
				when vnilai LIKE 'PT. Indah Karya' then return 'BUMN';
				when vnilai LIKE '%t.indah karya' then return 'BUMN';
				when vnilai LIKE '%t. indah karya' then return 'BUMN';
				when vnilai LIKE '%indra karya%' then return 'BUMN';
				when vnilai LIKE '%virama karya%' then return 'BUMN';
				when vnilai LIKE '%JO PT Virama Karya (Persero)%' then return 'BUMN';
				when vnilai LIKE '%PT. YODYA KARYA (PER%' then return 'BUMN';
				when vnilai LIKE '%jo PT. YODYA KARYA (PERSERO)%' then return 'BUMN';
				when vnilai LIKE '%T. BARATA INDON%' then return 'BUMN';
				when vnilai LIKE '%T. (PERSERO) SUCOFIND%' then return 'BUMN';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_bu2_copy
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_bu2_copy`;
delimiter ;;
CREATE FUNCTION `f_indikasi_bu2_copy`(vnilai varchar(200))
 RETURNS varchar(50) CHARSET latin1
BEGIN
    case 
        when vnilai LIKE '%rya (persero%' then return 'BUMN';
				when vnilai LIKE '%sia (persero%' then return 'BUMN';
				when vnilai LIKE '%telekomunikasi%' then return 'BUMN';
				when vnilai LIKE '%surveyor%' then return 'BUMN';
				when vnilai LIKE '%PT. SUCOFINDO%' then return 'BUMN';
				when vnilai LIKE '%NINDYA -%' then return 'BUMN';
				when vnilai LIKE '%nindya karya' then return 'BUMN';
				when vnilai LIKE '%nindya karya wilayah%' then return 'BUMN';
				when vnilai LIKE '%PT NINDYA KARYA (Persero) WILAYAH I%' then return 'BUMN';
				when vnilai LIKE '%erumaha%' then return 'BUMN';
				when vnilai LIKE '%T. waskita%' then return 'BUMN';
				when vnilai LIKE '%WASKITA (KS%' then return 'BUMN';
				when vnilai LIKE 'WASKITA - A%' then return 'BUMN';
				when vnilai LIKE '%IKA-BRANTAS J%' then return 'BUMN';
				when vnilai LIKE '%T. PP(Persero) (K%' then return 'BUMN';
				when vnilai LIKE '%yody%' then return 'BUMN';
				when vnilai LIKE '%jo PT. YODYA KARYA (PERSERO)%' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA' then return 'BUMN';
				when vnilai LIKE '%odya%' then return 'BUMN';
				when vnilai LIKE '%brantas ab%' then return 'BUMN';
				when vnilai LIKE '%amarta karya%' then return 'BUMN';
				when vnilai LIKE '%abipraya%' then return 'BUMN';
				when vnilai LIKE '%PT Wijaya Karya' then return 'BUMN';
				when vnilai LIKE '%PT.Wijaya Karya' then return 'BUMN';
				when vnilai LIKE '%PT. Wijaya Karya' then return 'BUMN';
				when vnilai LIKE 'WIKA -%' then return 'BUMN';
				when vnilai LIKE '%PP-HK%' then return 'BUMN';
				when vnilai LIKE '%PT. WIJAYA KARYA ( Pesero )%' then return 'BUMN';
				when vnilai LIKE '%PT. WIJAYA KARYA -%' then return 'BUMN';
				when vnilai LIKE '%PP-%' then return 'BUMN';
				when vnilai LIKE '%PT. Nindya Karya%' then return 'BUMN';
				when vnilai LIKE 'PT. ADHI KARYA%' then return 'BUMN';
				when vnilai LIKE 'Adhi-Nindy%' then return 'BUMN';
				when vnilai LIKE 'PT.ADHI KARYA%' then return 'BUMN';
				when vnilai LIKE 'PT. WIJAYA KARYA%' then return 'BUMN';
				when vnilai LIKE 'PP-HUTAMA%' then return 'BUMN';
				when vnilai LIKE 'WIKA ?%' then return 'BUMN';
				when vnilai LIKE 'HUTAMA-%' then return 'BUMN';
				when vnilai LIKE '%(JO) - PT. HUTAMA KARYA%' then return 'BUMN';
				when vnilai LIKE 'PT.Hutama Karya%' then return 'BUMN';
				when vnilai LIKE 'PT. Hutama Karya%' then return 'BUMN';
				when vnilai LIKE '%indra karya wilayah%' then return 'BUMN';
				when vnilai LIKE 'PT. Indah KaryaJO' then return 'BUMN';
				when vnilai LIKE 'PT. Indah Karya' then return 'BUMN';
				when vnilai LIKE '%t.indah karya' then return 'BUMN';
				when vnilai LIKE '%t. indah karya' then return 'BUMN';
				when vnilai LIKE '%indra karya%' then return 'BUMN';
				when vnilai LIKE '%virama karya%' then return 'BUMN';
				when vnilai LIKE '%JO PT Virama Karya (Persero)%' then return 'BUMN';
				when vnilai LIKE '%PT. YODYA KARYA (PER%' then return 'BUMN';
				when vnilai LIKE '%jo PT. YODYA KARYA (PERSERO)%' then return 'BUMN';
				when vnilai LIKE '%T. BARATA INDON%' then return 'BUMN';
				when vnilai LIKE '%T. (PERSERO) SUCOFIND%' then return 'BUMN';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_bumn
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_bumn`;
delimiter ;;
CREATE FUNCTION `f_indikasi_bumn`(vnilai varchar(200))
 RETURNS varchar(150) CHARSET latin1
BEGIN
    case 
        when vnilai LIKE 'PT ADHI KARYA (Persero) Tbk%' then return 'ADHI KARYA';
				when vnilai LIKE 'PT. ADHI KARYA%' then return 'ADHI KARYA';
				when vnilai LIKE 'PT.ADHI KARYA%' then return 'ADHI KARYA';
				when vnilai LIKE 'Adhi-Nindy%' then return 'ADHI KARYA';
				when vnilai LIKE '%T. waskita%' then return 'WASKITA KARYA';
				when vnilai LIKE 'WASKITA - A%' then return 'WASKITA KARYA';
				when vnilai LIKE '%IKA-BRANTAS J%' then return 'WASKITA KARYA';
				when vnilai LIKE '%odya%' then return 'YODYA KARYA';
				when vnilai LIKE '%0dya%' then return 'YODYA KARYA';
				when vnilai LIKE 'PT. YODA KARYA%' then return 'YODYA KARYA';
				when vnilai LIKE 'PT.Indrakarya (Pe%' then return 'INDRA KARYA';
				when vnilai LIKE '%indra karya%' then return 'INDRA KARYA';
				when vnilai LIKE '%indra karya wilayah%' then return 'INDRA KARYA';
				when vnilai LIKE '%indra karya' then return 'INDRA KARYA';
				when vnilai LIKE 'nindya%' then return 'NINDYA KARYA';
				when vnilai LIKE '%nindya karya' then return 'NINDYA KARYA';
				when vnilai LIKE 'PT. NINDYA KARYA%' then return 'NINDYA KARYA';
				when vnilai LIKE 'PT.NINDYA KARYA (Pers%' then return 'NINDYA KARYA';
				when vnilai LIKE '%PT NINDYA KARYA (Persero) WILAYAH I%' then return 'NINDYA KARYA';
				when vnilai LIKE '%nindya karya wilayah%' then return 'NINDYA KARYA';
				when vnilai LIKE '%PT. BINA KARYA (Persero)%' then return 'BINA KARYA';
				when vnilai LIKE '%PT BINA KARYA (PERS%' then return 'BINA KARYA';
				when vnilai LIKE '%PT. Bina Karya ( pers%' then return 'BINA KARYA';
				when vnilai LIKE 'PT. WIJAYA KARYA (Persero) Tbk' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. WIJAYA KARYA ' then return 'WIJAYA KARYA';
				when vnilai LIKE '%PT Wijaya Karya (Persero)%' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. WIJAYA KARYA (Persero)%' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. WIJAYA KARYA ( Pesero )%' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. Wijaya Karya Industri %' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. Wijjaya Karya (Perse%' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. WIJAYA KARYA BANGU%' then return 'WIJAYA KARYA';
				when vnilai LIKE '%PT. WIJAYA KARYA (Pers%' then return 'WIJAYA KARYA';
				when vnilai LIKE '%IKA-BRANTAS JO%' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. WIJAYA KARYA -%' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. WIJAYA KARYA' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. WIJAYA KARYA  ' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. WIJAYA KARYA,%' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. WIJAYA KARYA ( Persero ) Tb%' then return 'WIJAYA KARYA';
				when vnilai LIKE '%PT. WIJAYA KARYA' then return 'WIJAYA KARYA';
				when vnilai LIKE 'WIKA%' then return 'WIJAYA KARYA';
				when vnilai LIKE '%WASKITA (KS%' then return 'WIJAYA KARYA';
				when vnilai LIKE 'PT. HUTAMA KARYA%' then return 'HUTAMA KARYA';
				when vnilai LIKE 'PT.HUTAMA KARYA%' then return 'HUTAMA KARYA';
				when vnilai LIKE '%PT. HUTAMA KARYA (PERSERO%' then return 'HUTAMA KARYA'; 
				when vnilai LIKE '%-HK-%' then return 'HUTAMA KARYA';
				when vnilai LIKE 'HUTAMA-%' then return 'HUTAMA KARYA';
				when vnilai LIKE '%istaka karya%' then return 'ISTAKA KARYA';
				when vnilai LIKE '%T Indah Karya (Perser%' then return 'INDAH KARYA';
				when vnilai LIKE '%T. INDAH KARYA (PER%' then return 'INDAH KARYA';
				when vnilai LIKE '%t. indah karya' then return 'INDAH KARYA';
				when vnilai LIKE 'PT. Indah KaryaJO%' then return 'INDAH KARYA';
				when vnilai LIKE 'PT. Indah Karya' then return 'INDAH KARYA';
				when vnilai LIKE 'pp' then return 'PP';
				when vnilai LIKE 'PT. PP (Persero) Tbk%' then return 'PP';
				when vnilai LIKE 'PT. PP (PERSERO),TBK%' then return 'PP';
				when vnilai LIKE 'PT. PEMBANGUNAN PERUMA%' then return 'PP';
				when vnilai LIKE '%perumahan%' then return 'PP';
				when vnilai LIKE '%PP-%' then return 'PP';
				when vnilai LIKE 'PT. PP(Persero)%' then return 'PP';
				when vnilai LIKE '%virama kary%' then return 'VIRAMA KARYA';
				when vnilai LIKE '%JO PT Virama Karya (Persero)%' then return 'VIRAMA KARYA';
				when vnilai LIKE '%Virama Karya (Pers%' then return 'Virama Karya';
				when vnilai LIKE '%brantas ab%' then return 'BRANTAS ABIPRAYA';
				when vnilai LIKE '%abipray%' then return 'BRANTAS ABIPRAYA';
				when vnilai LIKE '%ucofindo%' then return 'SUCOFINDO';
				when vnilai LIKE '%T. (PERSERO) SUCOFIND%' then return 'SUCOFINDO';
				when vnilai LIKE '%urveyo%' then return 'SURVEYOR';
				when vnilai LIKE 'barata' then return 'BARATA';
				when vnilai LIKE 'PT.BARATA INDONESIA (PERS%' then return 'BARATA INDONESIA';
				when vnilai LIKE '%PT. AMARTA KARYA%' then return 'AMARTA';
				when vnilai LIKE '%elekomuni%' then return 'TELKOM';
				when vnilai LIKE '%T. BARATA INDONES%' then return 'BARATA INDONESIA';

        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_cluster_bu
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_cluster_bu`;
delimiter ;;
CREATE FUNCTION `f_indikasi_cluster_bu`(vnilai varchar(50))
 RETURNS varchar(50) CHARSET latin1
BEGIN
    case 
        when vnilai<2500000000 then return 'K (0 < 2,5M)';
				when vnilai>=2500000000 and vnilai<50000000000 then return 'M (2,5 < 50M)';
        when vnilai>=50000000000 and vnilai<100000000000 then return 'B1 (50 < 1000M)';                      
        when vnilai>=100000000000 then return 'B2 (> 100M)';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_jo
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_jo`;
delimiter ;;
CREATE FUNCTION `f_indikasi_jo`(vnilai varchar(300))
 RETURNS varchar(100) CHARSET latin1
BEGIN
    case 
        when vnilai LIKE '%KSO%' then return 'KSO/JO';
				when vnilai LIKE 'KSO%' then return 'KSO/JO';
				when vnilai LIKE '%KSO' then return 'KSO/JO';
				when vnilai LIKE '%JO%' then return 'KSO/JO';
				when vnilai LIKE '%J.O%' then return 'KSO/JO';
				when vnilai LIKE '%join%' then return 'KSO/JO';
				when vnilai LIKE '%Konsorsium%' then return 'KSO/JO';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_ki_bm
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_ki_bm`;
delimiter ;;
CREATE FUNCTION `f_indikasi_ki_bm`(vnilai varchar(300))
 RETURNS varchar(100) CHARSET latin1
BEGIN
    case 
					when vnilai LIKE '%Jalan%' then return 'JJ';
					when vnilai LIKE '%Longsor%' then return 'JJ';
					when vnilai LIKE '%Underpass%' then return 'JJ';
					when vnilai LIKE '%Overpass%' then return 'JJ';
					when vnilai LIKE '%Fly over%' then return 'JJ';
					when vnilai LIKE '%Akses%' then return 'JJ';
					when vnilai LIKE '%U-TURN%' then return 'JJ';
					when vnilai LIKE '%Alinyemen%' then return 'JJ';
					when vnilai LIKE '%JLN%' then return 'JJ';
					when vnilai LIKE '%Jl.%' then return 'JJ';
					when vnilai LIKE '%Lereng%' then return 'JJ';
					when vnilai LIKE '%Ruas%' then return 'JJ';
					when vnilai LIKE '%Bypass%' then return 'JJ';
					when vnilai LIKE '%Trase%' then return 'JJ';
					when vnilai LIKE '%Jembatan%' then return 'JJ';
					when vnilai LIKE '%JBT%' then return 'JJ';
					when vnilai LIKE '%ROAD%' then return 'JJ';
					when vnilai LIKE '%Pelebaran%' then return 'JJ';
					when vnilai LIKE '%Preservasi%' then return 'JJ';
					when vnilai LIKE '%Perservasi%' then return 'JJ';
					when vnilai LIKE '%ROAD%' then return 'JJ';
					when vnilai LIKE '%Rehabilitasi%' then return 'JJ';
					when vnilai LIKE '%Pemeliharaan%' then return 'JJ';
					when vnilai LIKE '%Rekonstruksi%' then return 'JJ';
					when vnilai LIKE '%Peningkatan%' then return 'JJ';
					when vnilai LIKE '%Penggantian%' then return 'JJ';

					when vnilai LIKE '%Gedung%' then return 'BPFP';
					when vnilai LIKE '%Rumah Dinas%' then return 'BPFP';
					when vnilai LIKE '%Kantor%' then return 'BPFP';
					when vnilai LIKE '%Dermaga%' then return 'BPFP';
					when vnilai LIKE '%Pagar%' then return 'BPFP';
					when vnilai LIKE '%GORR%' then return 'BPFP';
					when vnilai LIKE '%Ruang%' then return 'BPFP';
					when vnilai LIKE '%Workshop%' then return 'BPFP';
					when vnilai LIKE '%Pengamanan Aset%' then return 'BPFP';
					when vnilai LIKE '%Instalasi%' then return 'BPFP';
					when vnilai LIKE '%Lahan Parkir%' then return 'BPFP';
					when vnilai LIKE '%Drainase%' then return 'BPFP';
					when vnilai LIKE '%Batas Kota%' then return 'BPFP';
					when vnilai LIKE '%Bts%' then return 'BPFP';
					when vnilai LIKE '%Factory%' then return 'BPFP';
					when vnilai LIKE '%Bengkel%' then return 'BPFP';
					when vnilai LIKE '%Pembangunan%' then return 'BPFP';
					when vnilai LIKE '%banjir%' then return 'BPFP';
					when vnilai LIKE '%pengadaan%' then return 'BPFP';
					when vnilai LIKE '%penanganan%' then return 'BPFP';
					when vnilai LIKE '%pembebasan%' then return 'BPFP';
					when vnilai LIKE '%tanggap%' then return 'BPFP';
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_ki_ck
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_ki_ck`;
delimiter ;;
CREATE FUNCTION `f_indikasi_ki_ck`(vnilai varchar(300))
 RETURNS varchar(100) CHARSET latin1
BEGIN
    case 
					when vnilai LIKE '%%' then return '';
					
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_ki_pp
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_ki_pp`;
delimiter ;;
CREATE FUNCTION `f_indikasi_ki_pp`(vnilai varchar(300))
 RETURNS varchar(100) CHARSET latin1
BEGIN
    case 
					when vnilai LIKE '%%' then return '';
					
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_ki_sda
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_ki_sda`;
delimiter ;;
CREATE FUNCTION `f_indikasi_ki_sda`(vnilai varchar(300))
 RETURNS varchar(100) CHARSET latin1
BEGIN
    case 
					when vnilai LIKE '%%' then return '';
					
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_indikasi_myclanjut
-- ----------------------------
DROP FUNCTION IF EXISTS `f_indikasi_myclanjut`;
delimiter ;;
CREATE FUNCTION `f_indikasi_myclanjut`(vnilai varchar(300))
 RETURNS varchar(100) CHARSET latin1
BEGIN
    case 
        when vnilai >= 300 then return 'MYC Lanjutan';
        else
            return 'Tahunan';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_npwp_lokasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_npwp_lokasi`;
delimiter ;;
CREATE FUNCTION `f_npwp_lokasi`(vnilai varchar(100))
 RETURNS varchar(100) CHARSET latin1
BEGIN
    case 
				when vnilai BETWEEN '001' AND '093'  then return 'DKI JAKARTA';
				when vnilai BETWEEN '101' AND '107'  then return 'NANGGROE ACEH DARUSSALAM';
				when vnilai BETWEEN '111' AND '128'  then return 'SUMATERA UTARA';
				when vnilai BETWEEN '201' AND '205'  then return 'SUMATERA BARAT';
				when vnilai BETWEEN '211' AND '213'  then return 'RIAU';
				when vnilai = '216' then return 'RIAU';
				when vnilai = '218' then return 'RIAU';
				when vnilai BETWEEN '219' AND '222'  then return 'RIAU';
				when vnilai BETWEEN '214' AND '215'  then return 'KEPULAUAN RIAU';
				when vnilai = '217' then return 'KEPULAUAN RIAU';
				when vnilai BETWEEN '224' AND '225'  then return 'KEPULAUAN RIAU';
				when vnilai BETWEEN '301' AND '309'  then return 'SUMATERA SELATAN';
				when vnilai = '311' then return 'BENGKULU';
				when vnilai BETWEEN '312' AND '314'  then return 'SUMATERA SELATAN';
				when vnilai = '315' then return 'BANGKA BELITUNG';
				when vnilai BETWEEN '321' AND '326'  then return 'LAMPUNG';
				when vnilai BETWEEN '327' AND '328'  then return 'BENGKULU';
				when vnilai BETWEEN '331' AND '334'  then return 'JAMBI';
				when vnilai BETWEEN '401' AND '402'  then return 'BANTEN';
				when vnilai BETWEEN '403' AND '453'  then return 'JAWA BARAT';
				when vnilai BETWEEN '501' AND '533'  then return 'JAWA TENGAH';
				when vnilai BETWEEN '541' AND '545'  then return 'DI YOGYAKARTA';
				when vnilai BETWEEN '601' AND '657'  then return 'JAWA TIMUR';
				when vnilai BETWEEN '701' AND '706'  then return 'KALIMANTAN BARAT';
				when vnilai BETWEEN '711' AND '714'  then return 'KALIMANTAN TENGAH';
				when vnilai BETWEEN '721' AND '728'  then return 'KALIMANTAN TIMUR';
				when vnilai = '723' then return 'KALIMANTAN UTARA';
				when vnilai BETWEEN '731' AND '735'  then return 'KALIMANTAN SELATAN';
				when vnilai BETWEEN '801' AND '809'  then return 'SULAWESI SELATAN';
				when vnilai = '812' then return 'SULAWESI SELATAN';
				when vnilai = '811' then return 'SULAWESI TENGGARA';
				when vnilai BETWEEN '815' AND '816'  then return 'SULAWESI TENGGARA';
				when vnilai = '821' then return 'SULAWESI UTARA';
				when vnilai BETWEEN '823' AND '825'  then return 'SULAWESI UTARA';
				when vnilai = '822' then return 'GORONTALO';
				when vnilai BETWEEN '831' AND '834'  then return 'SULAWESI TENGAH';
				when vnilai BETWEEN '901' AND '908'  then return 'BALI';
				when vnilai BETWEEN '911' AND '915'  then return 'NUSA TENGGARA BARAT';
				when vnilai BETWEEN '916' AND '926'  then return 'NUSA TENGGARA TIMUR';
				when vnilai = '941' then return 'MALUKU';
				when vnilai BETWEEN '942' AND '943'  then return 'MALUKU UTARA';
				when vnilai = '951' then return 'PAPUA BARAT';
				when vnilai = '955' then return 'PAPUA BARAT';
				when vnilai = '956' then return 'PAPUA';
				when vnilai BETWEEN '951' AND '954'  then return 'PAPUA';
				
        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_pulau
-- ----------------------------
DROP FUNCTION IF EXISTS `f_pulau`;
delimiter ;;
CREATE FUNCTION `f_pulau`(vnilai varchar(100))
 RETURNS varchar(100) CHARSET latin1
BEGIN
    case 
				when vnilai = 'NANGGROE ACEH DARUSSALAM' then return 'SUMATERA';
				when vnilai = 'SUMATERA UTARA' then return 'SUMATERA';
				when vnilai = 'SUMATERA SELATAN' then return 'SUMATERA';
				when vnilai = 'SUMATERA BARAT' then return 'SUMATERA';
				when vnilai = 'RIAU' then return 'SUMATERA';
				when vnilai = 'BANGKA BELITUNG' then return 'SUMATERA';
				when vnilai = 'BENGKULU' then return 'SUMATERA';
				when vnilai = 'JAMBI' then return 'SUMATERA';
				when vnilai = 'KEPULAUAN RIAU' then return 'SUMATERA';
				when vnilai = 'LAMPUNG' then return 'SUMATERA';
				when vnilai = 'BANTEN' then return 'JAWA';
				when vnilai = 'DKI JAKARTA' then return 'JAWA';
				when vnilai = 'JAWA BARAT' then return 'JAWA';
				when vnilai = 'JAWA TENGAH' then return 'JAWA';
				when vnilai = 'DI YOGYAKARTA' then return 'JAWA';
				when vnilai = 'JAWA TIMUR' then return 'JAWA';
				when vnilai = 'KALIMANTAN BARAT' then return 'KALIMANTAN';
				when vnilai = 'KALIMANTAN TIMUR' then return 'KALIMANTAN';
				when vnilai = 'KALIMANTAN UTARA' then return 'KALIMANTAN';
				when vnilai = 'KALIMANTAN SELATAN' then return 'KALIMANTAN';
				when vnilai = 'KALIMANTAN TENGAH' then return 'KALIMANTAN';
				when vnilai = 'GORONTALO' then return 'SULAWESI';
				when vnilai = 'SULAWESI BARAT' then return 'SULAWESI';
				when vnilai = 'SULAWESI TENGAH' then return 'SULAWESI';
				when vnilai = 'SULAWESI UTARA' then return 'SULAWESI';
				when vnilai = 'SULAWESI SELATAN' then return 'SULAWESI';
				when vnilai = 'SULAWESI TENGGARA' then return 'SULAWESI';
				when vnilai = 'BALI' then return 'BALI & NUSA TENGGARA';
				when vnilai = 'NUSA TENGGARA BARAT' then return 'BALI & NUSA TENGGARA';
				when vnilai = 'NUSA TENGGARA TIMUR' then return 'BALI & NUSA TENGGARA';
				when vnilai = 'MALUKU' then return 'MALUKU & MALUKU UTARA';
				when vnilai = 'MALUKU UTARA' then return 'MALUKU & MALUKU UTARA';
				when vnilai = 'PAPUA' then return 'PAPUA';
				when vnilai = 'PAPUA BARAT' then return 'PAPUA';

        else
            return '-';
    end case;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for f_validasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_validasi`;
delimiter ;;
CREATE FUNCTION `f_validasi`(vnilai varchar(1000))
 RETURNS varchar(1000) CHARSET latin1
BEGIN
    case 
        
				when vnilai LIKE 'PT%' then return 'valid';
				when vnilai LIKE 'CV%' then return 'valid';
				when vnilai LIKE '%PT%' then return 'valid';
				when vnilai LIKE '%CV%' then return 'valid';
				when vnilai LIKE 'PT%' then return 'valid';
				when vnilai LIKE 'CV%' then return 'valid';
				when vnilai LIKE 'dek' then return 'valid';
				when vnilai LIKE '%kons%' then return 'valid';
				when vnilai LIKE '%ekni%' then return 'valid';
        else
            return '-';
    end case;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
