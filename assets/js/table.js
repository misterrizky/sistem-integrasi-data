let dom = "<'row'" +
"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
">" +

"<'table-responsive'tr>" +

"<'row'" +
"<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
"<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
">";
let language = {
    search: "Cari",
    lengthMenu: "Menampilkan _MENU_ data per halaman",
    zeroRecords: "Tidak ada yang ditemukan - maaf",
    info: "Menampilkan halaman _PAGE_ dari _PAGES_",
    infoEmpty: "Tidak ada data yang tersedia",
    infoFiltered: "(disaring dari _MAX_ total data)"
};
$(document).ready(function() {
    let table_emon = $('#table_emon').DataTable( {
        responsive: true,
        language: language,
        dom: dom,
        ajax: {
            url: APP_URL+'table/emon.php',
            type: 'POST'
        },
        order: [[ 2, 'asc' ]],
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title:"Tgl Emon",
                data: "tanggal"
            },
            {
                title:"Nama",
                data: "nama"
            },
            {
                title:"Tabel",
                data: "tabel"
            },
            {
                title:"Status",
                data: "st"
            },
            {
                title:"Format",
                data: "format"
            },
            {
                title:"Dibuat oleh",
                data: "created_by"
            },
            {
                title:"Diupload oleh",
                data: "uploaded_by"
            },
            {
                title:"Dicleansing oleh",
                data: "cleansing_by"
            },
            {
                title:"Tanggal buat",
                data: "created_at"
            },
            {
                title:"Tanggal upload",
                data: "uploaded_at"
            },
            {
                title:"Tanggal cleansing",
                data: "cleansing_at"
            },
            {
                title: 'Aksi',
                data: 'id',
                width: '15%',
                sClass: 'text-center',
                orderable: 'false',
                mRender: function (data,type,row) {
                    if(row.tabel == null){
                        return `<a href="javascript:;" onclick="handle_modal('upload_emon','#ModalUploadEmon','#ContentUploadEmon','`+row.id+`');">Upload</a>`;
                    }else{
                        return `<a target="_blank" href="`+APP_URL+`emon/preview/`+row.tabel+`">Lihat Data</a>`;
                    }
                }
            }
        ]
    } );

    table_emon.on('order.dt search.dt', function(){
        table_emon.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();

    let table_emon_preview_lama = $('#table_emon_preview_lama').DataTable( {
        responsive: true,
        language: language,
        dom: dom,
        ajax: {
            url: APP_URL+'table/emon_preview_lama.php?code='+code,
            type: 'POST'
        },
        order: [[ 2, 'asc' ]],
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title:"Kode & Nama Paket",
                data: "paket"
            },
            {
                title:"Nomor & Jenis Kontrak",
                data: "kontrak"
            },
            {
                title:"Provinsi",
                data: "provinsi"
            },
            {
                title:"Kategori",
                data: "kategori"
            },
            {
                title:"NPWP & Nama Rekanan",
                data: "rekanan"
            },
            {
                title:"Alamat",
                data: "alamat"
            },
            {
                title:"Tanggal Kontrak",
                data: "tkontrak"
            },
            {
                title:"Tanggal Selesai",
                data: "selesai"
            },
            {
                title:"PAGU RPM",
                data: "pagu_rpm"
            },
            {
                title:"PAGU PLN",
                data: "pagu_pln"
            },
            {
                title:"PAGU Total",
                data: "pagu_total"
            },
            {
                title:"NKON RPM",
                data: "nkon_rpm"
            },
            {
                title:"NKON PLN",
                data: "nkon_pln"
            },
            {
                title:"NKON Total",
                data: "nkon_total"
            },
            {
                title:"Unor",
                data: "unor"
            },
            {
                title:"TA",
                data: "ta"
            },
        ]
    } );

    table_emon_preview_lama.on('order.dt search.dt', function(){
        table_emon_preview_lama.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_emon_cleansing_lama = $('#table_emon_cleansing_lama').DataTable( {
        responsive: true,
        language: language,
        dom: dom,
        ajax: {
            url: APP_URL+'table/emon_cleansing_lama.php?code='+code+'&cleansing='+hal,
            type: 'POST'
        },
        order: [[ 2, 'asc' ]],
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title:"Kode & Nama Paket",
                data: "paket"
            },
            {
                title:"Nomor & Jenis Kontrak",
                data: "kontrak"
            },
            {
                title:"Provinsi",
                data: "provinsi"
            },
            {
                title:"Kategori",
                data: "kategori"
            },
            {
                title:"NPWP & Nama Rekanan",
                data: "rekanan"
            },
            {
                title:"Alamat",
                data: "alamat"
            },
            {
                title:"Tanggal Kontrak",
                data: "tkontrak"
            },
            {
                title:"Tanggal Selesai",
                data: "selesai"
            },
            {
                title:"PAGU RPM",
                data: "pagu_rpm"
            },
            {
                title:"PAGU PLN",
                data: "pagu_pln"
            },
            {
                title:"PAGU Total",
                data: "pagu_total"
            },
            {
                title:"NKON RPM",
                data: "nkon_rpm"
            },
            {
                title:"NKON PLN",
                data: "nkon_pln"
            },
            {
                title:"NKON Total",
                data: "nkon_total"
            },
            {
                title:"Unor",
                data: "unor"
            },
            {
                title:"TA",
                data: "ta"
            },
        ]
    } );

    table_emon_cleansing_lama.on('order.dt search.dt', function(){
        table_emon_cleansing_lama.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_emon_preview_baru = $('#table_emon_preview_baru').DataTable( {
        responsive: true,
        language: language,
        dom: dom,
        ajax: {
            url: APP_URL+'table/emon_preview_baru.php?code='+code,
            type: 'POST'
        },
        order: [[ 2, 'asc' ]],
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title:"Provinsi",
                data: "provinsi"
            },
            {
                title:"Kode & Nama Paket",
                data: "paket"
            },
            {
                title:"Kategori",
                data: "kategori"
            },
            {
                title:"Jenis Kontrak",
                data: "kontrak"
            },
            {
                title:"Rencana Pengumuman",
                data: "rencana_pengumuman"
            },
            {
                title:"RPM",
                data: "rpm"
            },
            {
                title:"RPM",
                data: "rpm"
            },
            {
                title:"SBSN",
                data: "sbsn"
            },
            {
                title:"PHLN",
                data: "phln"
            },
            {
                title:"Barang",
                data: "barang"
            },
            {
                title:"Modal",
                data: "modal"
            },
            {
                title:"DIPA",
                data: "dipa"
            },
            {
                title:"Pengadaan",
                data: "pengadaan"
            },
            {
                title:"UNOR",
                data: "unor"
            },
            {
                title:"Check Tayang",
                data: "check_tayang"
            },
            {
                title:"SIRUP",
                data: "sirup"
            },
            {
                title:"Tanggal SIPBJ",
                data: "tanggal_sipbj"
            },
            {
                title:"Status SIPBJ",
                data: "status_sipbj"
            },
            {
                title:"Kode SPSE",
                data: "kode_spse"
            },
            {
                title:"Pengumuman Lelang",
                data: "tanggal_pengumuman_lelang"
            },
            {
                title:"Penetapan Pemenang",
                data: "tanggal_penetapan_pemenang"
            },
            {
                title:"Rencana Kontrak",
                data: "tanggal_rencana_kontrak"
            },
            {
                title:"Status",
                data: "status"
            },
            {
                title:"TA",
                data: "ta"
            },
        ]
    } );

    table_emon_preview_baru.on('order.dt search.dt', function(){
        table_emon_preview_baru.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_emon_cleansing_baru = $('#table_emon_cleansing_baru').DataTable( {
        responsive: true,
        language: language,
        dom: dom,
        ajax: {
            url: APP_URL+'table/emon_cleansing_baru.php?code='+code+'&cleansing='+hal,
            type: 'POST'
        },
        order: [[ 2, 'asc' ]],
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title:"Provinsi",
                data: "provinsi"
            },
            {
                title:"Kode & Nama Paket",
                data: "paket"
            },
            {
                title:"Kategori",
                data: "kategori"
            },
            {
                title:"Jenis Kontrak",
                data: "kontrak"
            },
            {
                title:"Rencana Pengumuman",
                data: "rencana_pengumuman"
            },
            {
                title:"RPM",
                data: "rpm"
            },
            {
                title:"RPM",
                data: "rpm"
            },
            {
                title:"SBSN",
                data: "sbsn"
            },
            {
                title:"PHLN",
                data: "phln"
            },
            {
                title:"Barang",
                data: "barang"
            },
            {
                title:"Modal",
                data: "modal"
            },
            {
                title:"DIPA",
                data: "dipa"
            },
            {
                title:"Pengadaan",
                data: "pengadaan"
            },
            {
                title:"UNOR",
                data: "unor"
            },
            {
                title:"Check Tayang",
                data: "check_tayang"
            },
            {
                title:"SIRUP",
                data: "sirup"
            },
            {
                title:"Tanggal SIPBJ",
                data: "tanggal_sipbj"
            },
            {
                title:"Status SIPBJ",
                data: "status_sipbj"
            },
            {
                title:"Kode SPSE",
                data: "kode_spse"
            },
            {
                title:"Pengumuman Lelang",
                data: "tanggal_pengumuman_lelang"
            },
            {
                title:"Penetapan Pemenang",
                data: "tanggal_penetapan_pemenang"
            },
            {
                title:"Rencana Kontrak",
                data: "tanggal_rencana_kontrak"
            },
            {
                title:"Status",
                data: "status"
            },
            {
                title:"TA",
                data: "ta"
            },
        ]
    } );

    table_emon_cleansing_baru.on('order.dt search.dt', function(){
        table_emon_cleansing_baru.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();

    let table_lihat_emon_pbj_paket = $('#table_lihat_emon_pbj_paket').DataTable({
        responsive: true,
        language: language,
        dom: dom,
        processing: true,
        autoWidth: true,
        // serverSide: true,
        ajax: {
            url: APP_URL + 'table/emon_pbj_paket.php?table='+code,
            type: 'POST',
        },
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title: 'Jenis Pengadaan',
                data: 'kategori'
            },
            {
                title: 'Jumlah',
                data: 'total_paket'
            }
        ],
    });
    table_lihat_emon_pbj_paket.on('order.dt search.dt', function(){
        table_lihat_emon_pbj_paket.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_lihat_emon_pk_paket = $('#table_lihat_emon_pk_paket').DataTable({
        responsive: true,
        language: language,
        dom: dom,
        processing: true,
        autoWidth: true,
        // serverSide: true,
        ajax: {
            url: APP_URL + 'table/emon_pk_paket.php?table='+code,
            type: 'POST',
        },
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title: 'Cluster',
                data: 'cluster_pemaketan_kontraktor'
            },
            {
                title: 'Jumlah',
                data: 'total_paket'
            }
        ],
    });
    table_lihat_emon_pk_paket.on('order.dt search.dt', function(){
        table_lihat_emon_pk_paket.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_lihat_emon_pk_bumn_swasta = $('#table_lihat_emon_pk_bumn_swasta').DataTable({
        responsive: true,
        language: language,
        dom: dom,
        processing: true,
        autoWidth: true,
        // serverSide: true,
        ajax: {
            url: APP_URL + 'table/emon_pk_bumn_swasta.php?table='+code,
            type: 'POST',
        },
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title: 'Pelaksana PK',
                data: 'BUMN_1'
            },
            {
                title: '2015',
                data: 'lima_belas'
            },
            {
                title: '2016',
                data: 'enam_belas'
            },
            {
                title: '2017',
                data: 'tujuh_belas'
            },
            {
                title: '2018',
                data: 'lapan_belas'
            },
            {
                title: '2019',
                data: 'bilan_belas'
            }
        ],
    });
    table_lihat_emon_pk_bumn_swasta.on('order.dt search.dt', function(){
        table_lihat_emon_pk_bumn_swasta.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_lihat_emon_pk_bumn_swasta_100 = $('#table_lihat_emon_pk_bumn_swasta_100').DataTable({
        responsive: true,
        language: language,
        dom: dom,
        processing: true,
        autoWidth: true,
        // serverSide: true,
        ajax: {
            url: APP_URL + 'table/emon_pk_bumn_swasta_100.php?table='+code,
            type: 'POST',
        },
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title: 'Pelaksana PK',
                data: 'BUMN_1'
            },
            {
                title: '2015',
                data: 'lima_belas'
            },
            {
                title: '2016',
                data: 'enam_belas'
            },
            {
                title: '2017',
                data: 'tujuh_belas'
            },
            {
                title: '2018',
                data: 'lapan_belas'
            },
            {
                title: '2019',
                data: 'bilan_belas'
            }
        ],
    });
    table_lihat_emon_pk_bumn_swasta_100.on('order.dt search.dt', function(){
        table_lihat_emon_pk_bumn_swasta_100.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_lihat_emon_pk_paket_malas = $('#table_lihat_emon_pk_paket_malas').DataTable({
        responsive: true,
        language: language,
        dom: dom,
        processing: true,
        autoWidth: true,
        // serverSide: true,
        ajax: {
            url: APP_URL + 'table/emon_pk_paket_malas.php?table='+code,
            type: 'POST',
        },
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title: 'Rekanan',
                data: 'rekanan1'
            },
            {
                title: 'Jumlah',
                data: 'total_paket'
            }
        ],
    });
    table_lihat_emon_pk_paket_malas.on('order.dt search.dt', function(){
        table_lihat_emon_pk_paket_malas.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_lihat_emon_pk_nilai_malas = $('#table_lihat_emon_pk_nilai_malas').DataTable({
        responsive: true,
        language: language,
        dom: dom,
        processing: true,
        autoWidth: true,
        // serverSide: true,
        ajax: {
            url: APP_URL + 'table/emon_pk_nilai_malas.php?table='+code,
            type: 'POST',
        },
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title: 'Rekanan',
                data: 'rekanan1'
            },
            {
                title: 'Jumlah',
                data: 'total_nilai'
            }
        ],
    });
    table_lihat_emon_pk_nilai_malas.on('order.dt search.dt', function(){
        table_lihat_emon_pk_nilai_malas.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_lihat_emon_jk_paket = $('#table_lihat_emon_jk_paket').DataTable({
        responsive: true,
        language: language,
        dom: dom,
        processing: true,
        autoWidth: true,
        // serverSide: true,
        ajax: {
            url: APP_URL + 'table/emon_jk_paket.php?table='+code,
            type: 'POST',
        },
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title: 'Cluster',
                data: 'cluster_pemaketan_konsultan'
            },
            {
                title: 'Jumlah',
                data: 'total_paket'
            }
        ],
    });
    table_lihat_emon_jk_paket.on('order.dt search.dt', function(){
        table_lihat_emon_jk_paket.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();

    let table_lihat_emon_pbj_nilai = $('#table_lihat_emon_pbj_nilai').DataTable({
        responsive: true,
        language: language,
        dom: dom,
        processing: true,
        // autoWidth: true,
        // serverSide: true,
        ajax: {
            url: APP_URL + 'table/emon_pbj_nilai.php?table='+code,
            type: 'POST',
        },
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title: 'Jenis Pengadaan',
                data: 'kategori'
            },
            {
                title: 'Jumlah',
                data: 'total_nilai'
            }
        ],
    });
    table_lihat_emon_pbj_nilai.on('order.dt search.dt', function(){
        table_lihat_emon_pbj_nilai.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_lihat_emon_pk_nilai = $('#table_lihat_emon_pk_nilai').DataTable({
        responsive: true,
        language: language,
        dom: dom,
        processing: true,
        // autoWidth: true,
        // serverSide: true,
        ajax: {
            url: APP_URL + 'table/emon_pk_nilai.php?table='+code,
            type: 'POST',
        },
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title: 'Cluster',
                data: 'cluster_pemaketan_kontraktor'
            },
            {
                title: 'Jumlah',
                data: 'total_nilai'
            }
        ],
    });
    table_lihat_emon_pk_nilai.on('order.dt search.dt', function(){
        table_lihat_emon_pk_nilai.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
    let table_lihat_emon_jk_nilai = $('#table_lihat_emon_jk_nilai').DataTable({
        responsive: true,
        language: language,
        dom: dom,
        processing: true,
        // autoWidth: true,
        // serverSide: true,
        ajax: {
            url: APP_URL + 'table/emon_jk_nilai.php?table='+code,
            type: 'POST',
        },
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title: 'Cluster',
                data: 'cluster_pemaketan_konsultan'
            },
            {
                title: 'Jumlah',
                data: 'total_nilai'
            }
        ],
    });
    table_lihat_emon_jk_nilai.on('order.dt search.dt', function(){
        table_lihat_emon_jk_nilai.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();

    let table_pengguna = $('#table_pengguna').DataTable( {
        language: language,
        dom:dom,
        ajax: {
            url: APP_URL+'table/pengguna.php',
            type: 'POST'
        },
        order: [[ 2, 'asc' ]],
        columns: [
            {
                title:"No.",
                data:null,
                sClass:"text-center",
                orderable:false,
            },
            {
                title:"ID Pengguna",
                data: "username"
            },
            {
                title:"Nama",
                data: "nama"
            },
            {
                title:"Email",
                data: "email"
            },
        ]
    });

    table_pengguna.on('order.dt search.dt', function(){
        table_pengguna.column(0,{search:'applied', order:'applied'}).nodes().each(function(cell,i){
            cell.innerHTML=i+1;
        });
    }).draw();
});