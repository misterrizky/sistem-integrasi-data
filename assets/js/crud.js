let pathArray = location.pathname.split('/');
let modul = pathArray[2];
let hal = pathArray[3];
let code = pathArray[4];
let clear_timer;
if(!code){
    // get_content(modul + '.php?page=',1);
}else{
    get_info(modul + '.php', code);
    get_counter(modul + '.php', code);
    // load_content(hal,modul + '_' + hal + '.php?code=' + code + '&page=',1);
}
function get_info(file,param){
    // loading_div('#content_info');
    $.get(APP_URL + 'info/' + file, {params:param}, function(result) {
        $('#content_info').html(result);
        // loaded_div('#content_info');
    }, "html");
}
function get_counter(file,param){
    // loading_div('#content_counter');
    $.get(APP_URL + 'counter/' + file, {params:param}, function(result) {
        $('#content_counter').html(result);
        // loaded_div('#content_counter');
    }, "html");
}
function handle_modal(param,modal,content,id){
    $.ajax({
        type: "POST",
        url: APP_URL + 'crud/modal.php',
        data: {
            modal: param,
            id: id
        },
        success: function (html) {
            $(content).html(html);
            $(modal).modal('show');
        },
        error: function () {
            $(content).html('<h3>Ajax Bermasalah !!!</h3>')
        },
    });
}
function handle_save_modal(tombol,form,modal,table){
    $("#" + tombol).submit(function () {
        return false;
    });
    let data = $(form).serialize() + "&" + tombol + "=";
    $('#' + tombol).attr("data-kt-indicator", "on");
    $('#' + tombol).prop("disabled", true);
    $('#' + tombol).html("Harap Tunggu...");
    $.ajax({
        type: "POST",
        url: APP_URL + 'crud/create.php',
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.alert == "success"){
                setTimeout(function () {
                    $('#' + tombol).removeAttr("data-kt-indicator");
                    $('#' + tombol).prop("disabled", false);
                    $('#' + tombol).html("Simpan");
                    Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn btn-primary" } }).then(function (t) {
                        // 
                    }).then(function() {
                        $(form)[0].reset();
                        $(table).DataTable().ajax.reload();
                        $(modal).modal('toggle');
                    });
                }, 2e3);
            } else {
                Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn btn-primary" } }).then(function (t) {
                    // 
                }).then(function() {
                    $('#' + tombol).removeAttr("data-kt-indicator");
                    $('#' + tombol).prop("disabled", false);
                    $('#' + tombol).html("Simpan");
                });
            }
        },
    });
}
function handle_upload_modal(tombol, form, title, process, process_data, total_data){
    $(document).one('submit', form, function (e) {
        let data = new FormData(this);
        data.append(tombol, tombol);
        $('#' + tombol).attr("data-kt-indicator", "on");
        $('#' + tombol).prop("disabled", true);
        $('#' + tombol).html("Harap Tunggu...");
        $(process).css('display', 'block');
        $.ajax({
            type: 'POST',
            url: APP_URL + 'crud/upload.php',
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            resetForm: true,
            processData: false,
            dataType: 'json',
            success: function (response) {
                if (response.alert=="success") {
                    $(process_data).text(0);
                    $(total_data).text(response.total_data);
                    start_import(tombol,form,process);
                    clear_timer = setInterval(get_import_data, 2000);
                } else {
                    error_message(response.message);
                    setTimeout(function () {
                        $('#' + tombol).removeAttr("data-kt-indicator");
                        $('#' + tombol).prop("disabled", false);
                        $('#' + tombol).html("Simpan");
                    }, 2000);
                }
            },
        });
        return false;
    });
}
function start_import(tombol,form,process){
    let data = $(form).serialize() + "&" + tombol + "=";
    $(process).css('display', 'block');
    $.ajax({
        type: "POST",
        url: APP_URL + "crud/import_"+modul+".php",
        data: data,
        success:function(){
        }
    });
}
function get_import_data(){
    let tabel = $("input[name='tabel']").val();
    $.ajax({
        url: APP_URL + "crud/process_"+modul+".php?tabel="+tabel,
        success:function(response){
            let totaldata = $("#total_data_" + modul).text();
            let width = Math.round((response/totaldata)*100);
            $("#progress_data_" + modul).text(response);
            $(".progress-bar").css('width', width + '%');
            if(width >= 100){
                success_message('File berhasil di upload');
                clearInterval(clear_timer);
                $("#form_upload_" + modul)[0].reset();
                $('#upload_'+modul).prop("disabled", false);
                $('#upload_'+modul).html("Upload");
                if(modul == "emon"){
                    $("#table_emon").DataTable().ajax.reload();
                    $("#ModalUploadEmon").modal('hide');
                }
            }
        }
    })
}
function handle_delete(){
    // 
}
function handle_confirm(param,id){
    $.confirm({
        animationSpeed: 1000,
        animation: 'zoom',
        closeAnimation: 'scale',
        animateFromElement: false,
        columnClass: 'medium',
        title: 'Konfirmasi',
        content: 'Apakah anda yakin ?',
        icon: 'fa fa-question',
        theme: 'material',
        closeIcon: true,
        type: 'orange',
        autoClose: 'Tidak|5000',
        buttons: {
            'Ya': {
                btnClass: 'btn-red any-other-class',
                action: function(){
                    $.ajax({
                        type:"POST",
                        url:APP_URL+'crud/confirm.php',
                        data:{param:param,id:id},
                        dataType: "json",
                        success:function(result){
                            if (result.alert == "success") {
                                success_message(result.message);
                                get_info(modul + '.php', code);
                                get_counter(modul + '.php', code);
                            }else{
                                error_message(result.message);
                            }
                        },
                    });
                }
            },
            'Tidak': {
                btnClass: 'btn-blue', // multiple classes.
                // ...
            }
        }
    });
}