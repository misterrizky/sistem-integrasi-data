function username(obj) {
    $('#' + obj).bind('keypress', function (event) {
        var regex = new RegExp("^[A-Za-z0-9]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
}
username('username');
$("#username").focus();
$("#login_form").on('keydown', 'input', function (event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-login'));
        var val = $($this).val();

        if(val == 1){
            $('[data-login="' + (index + 1).toString() + '"]').focus();
        }else{
            $('#tombol_login').trigger("click");
        }
    }
});
"use strict";
var KTSigninGeneral = (function () {
    var t, e, i;
    return {
        init: function () {
            (t = document.querySelector("#login_form")),
                (e = document.querySelector("#tombol_login")),
                (i = FormValidation.formValidation(t, {
                    fields: {
                        username: { validators: { notEmpty: { message: "ID Pengguna diperlukan" } } },
                        password: { validators: { notEmpty: { message: "Kata sandi diperlukan" } } },
                    },
                    plugins: { trigger: new FormValidation.plugins.Trigger(), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row" }) },
                })),
                e.addEventListener("click", function (n) {
                    n.preventDefault(),
                        i.validate().then(function (i) {
                            login_data = {
                                login: '',
                                username: $("#username").val(),
                                password: $("#password").val()
                            };
                            if(i == "Valid"){
                                e.setAttribute("data-kt-indicator", "on"),e.disabled = !0;
                                $.ajax({
                                    type: "POST",
                                    url: APP_URL + "crud/auth.php",
                                    data: login_data,
                                    dataType: 'json',
                                    success: function (response) {
                                        if (response.alert=="success") {
                                            setTimeout(function () {
                                                e.removeAttribute("data-kt-indicator"),
                                                    (e.disabled = !1),
                                                    Swal.fire({ text: response.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn btn-primary" } }).then(function (t) {
                                                        e.isConfirmed && (t.reset());
                                                    }).then(function() {
                                                        location.reload();
                                                    });
                                            }, 2e3);
                                        } else {
                                            setTimeout(function () {
                                                e.removeAttribute("data-kt-indicator"),
                                                    (e.disabled = !1),
                                                    Swal.fire({ text: response.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok", customClass: { confirmButton: "btn btn-primary" } }).then(function (t) {
                                                        // t.isConfirmed && (e.reset());
                                                    });
                                            }, 2e3);
                                        }
                                    }
                                });
                            }else{
                                Swal.fire({
                                    text: "Maaf, sepertinya ada beberapa kesalahan yang terdeteksi, silakan coba lagi.",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok",
                                    customClass: { confirmButton: "btn btn-primary" },
                                }); 
                            }
                        });
                });
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTSigninGeneral.init();
});
