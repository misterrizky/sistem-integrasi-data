<script type="text/javascript">
    let APP_URL = "<?php echo APP_URL;?>";
</script>
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="<?php echo APP_ASSETS;?>plugins/global/plugins.bundle.js"></script>
<script src="<?php echo APP_ASSETS;?>js/scripts.bundle.js"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<?php
if(isset($_SESSION['token'])){
?>
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="<?php echo APP_ASSETS;?>plugins/custom/datatables/datatables.bundle.js"></script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="<?php echo BASE_ASSETS;?>js/confirm.min.js"></script>
<script src="<?php echo BASE_ASSETS;?>js/regex.js"></script>
<script src="<?php echo BASE_ASSETS;?>js/plugin.js"></script>
<script src="<?php echo BASE_ASSETS;?>js/table.js"></script>
<script src="<?php echo BASE_ASSETS;?>js/crud.js"></script>
<script>
    obj_datepicker('#tanggal_emon');
</script>
<!--end::Page Custom Javascript-->
<?php
}else{
?>
<script src="<?php echo BASE_ASSETS;?>js/auth.js"></script>
<?php
}
?>