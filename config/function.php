<?php
function ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
function getUserIP(){
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP)){
        $ip = $client;
    }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }
    return $ip;

    // HOW TO CALL
    // $user_ip = getUserIP();
    // echo $user_ip;
}
function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = '';
    $secret_iv = '';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}
function berlalu($timestamp){
    $time_ago        = strtotime($timestamp);
    $current_time    = time();
    $time_difference = $current_time - $time_ago;
    $seconds         = $time_difference;
    
    $minutes = round($seconds / 60);
    $hours   = round($seconds / 3600);
    $days    = round($seconds / 86400);
    $weeks   = round($seconds / 604800);
    $months  = round($seconds / 2629440);
    $years   = round($seconds / 31553280);

    if ($seconds <= 60):
        return "Just Now";
    elseif ($minutes <= 60):
        if ($minutes == 1):
            return "one minute ago";
        else:
            return "$minutes minutes ago";
        endif;
    elseif ($hours <= 24):
        if ($hours == 1):
            return "an hour ago";
        else:
            return "$hours hrs ago";
        endif;
    elseif ($days <= 7):
        if ($days == 1):
            return "yesterday";
        else:
            return "$days days ago";
        endif;
    elseif ($weeks <= 4.3):
        if ($weeks == 1):
            return "a week ago";
        else:
            return "$weeks weeks ago";
        endif;
    elseif ($months <= 12):
        if ($months == 1):
            return "a month ago";
        else:
            return "$months months ago";
        endif;
    else:
        if ($years == 1):
            return "one year ago";
        else:
            return "$years years ago";
        endif;
    endif;
}

function tanggal($mdate, $datestr = '%d %M %Y',$str_rep='') {
    if ($mdate != '' && $mdate != "0000-00-00")
    {
        $timestamp 	= strtotime($mdate);
        $datestr 	= str_replace('%\\', '', preg_replace("/([a-z]+?){1}/i", "\\\\\\1", $datestr));
            
        return date($datestr, $timestamp);
    }else
    {
        return $str_rep;
    }
}

function thousand($num=0,$dec_digit = 0,$decimal_spar='.',$thausand_spar = ','){
    if(!$num){
        $num = 0;
    }
    return number_format($num,$dec_digit,$decimal_spar,$thausand_spar);
}
function pbkdf2(){
    $password = "password";
    $iterations = 1000;

    // Generate a random IV using openssl_random_pseudo_bytes()
    // random_bytes() or another suitable source of randomness
    $salt = openssl_random_pseudo_bytes(16);

    $hash = hash_pbkdf2("sha256", $password, $salt, $iterations, 20);
    echo $hash;
}
function random_pass(){
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function GUID(){
    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

function num2text($text='0'){
    $result = str_replace(",", "", $text);
    $result = $result?$result:'0';
    return $result;
}
function encode($str=''){
    $result = base64_encode($str);		
    $result = base64_encode($result);
    //$result = strtr($result, '+/', '-_');
    return $result;
}
function decode($str=''){
    $result = base64_decode($str);		
    $result = base64_decode($result);		
    return $result;
}
function jml_minggu($tgl_awal, $tgl_akhir){
	$detik = 24 * 3600;
	$tgl_awal = strtotime($tgl_awal);
	$tgl_akhir = strtotime($tgl_akhir);

	$minggu = 0;
	for ($i=$tgl_awal; $i < $tgl_akhir; $i += $detik){
		if (date("w", $i) == "0"){
			$minggu++;
		}
	}
	return $minggu;
}
function bulan_romawi($str){
	switch ($str) {
		case '01': return 'I'; break;
		case '02': return 'II'; break;
		case '03': return 'III'; break;
		case '04': return 'IV'; break;
		case '05': return 'V'; break;
		case '06': return 'VI'; break;
		case '07': return 'VII'; break;
		case '08': return 'VIII'; break;
		case '09': return 'IX'; break;
		case '10': return 'X'; break;
		case '11': return 'XI'; break;
		case '12': return 'XII'; break;
	}
}
function nama_bulan($str,$tp=0){
    if($tp==1){
        switch ($str) {
            case '01': return 'Jan'; break;
            case '02': return 'Feb'; break;
            case '03': return 'Mar'; break;
            case '04': return 'Apr'; break;
            case '05': return 'Mei'; break;
            case '06': return 'Jun'; break;
            case '07': return 'Jul'; break;
            case '08': return 'Ags'; break;
            case '09': return 'Sep'; break;
            case '10': return 'Okt'; break;
            case '11': return 'Nov'; break;
            case '12': return 'Des'; break;
        }
    }else{
        switch ($str) {
            case '01': return 'Januari'; break;
            case '02': return 'Februari'; break;
            case '03': return 'Maret'; break;
            case '04': return 'April'; break;
            case '05': return 'Mei'; break;
            case '06': return 'Juni'; break;
            case '07': return 'Juli'; break;
            case '08': return 'Agustus'; break;
            case '09': return 'September'; break;
            case '10': return 'Oktober'; break;
            case '11': return 'November'; break;
            case '12': return 'Desember'; break;
        }
    }
}
function month_name($str,$tp=0){
    if($tp==1){
        switch ($str) {
            case '01': return 'Jan'; break;
            case '02': return 'Feb'; break;
            case '03': return 'Mar'; break;
            case '04': return 'Apr'; break;
            case '05': return 'May'; break;
            case '06': return 'Jun'; break;
            case '07': return 'Jul'; break;
            case '08': return 'Agst'; break;
            case '09': return 'Sep'; break;
            case '10': return 'Oct'; break;
            case '11': return 'Nov'; break;
            case '12': return 'Dec'; break;
        }
    }else{
        switch ($str) {
            case '01': return 'January'; break;
            case '02': return 'February'; break;
            case '03': return 'March'; break;
            case '04': return 'April'; break;
            case '05': return 'May'; break;
            case '06': return 'June'; break;
            case '07': return 'July'; break;
            case '08': return 'August'; break;
            case '09': return 'September'; break;
            case '10': return 'October'; break;
            case '11': return 'November'; break;
            case '12': return 'December'; break;
        }
    }
}
function desimal($str){
	return rtrim(rtrim($str,'0'),'.');
}
function format_tanggal_db($inputDate) {
    $return_value = 'NULL';
    if ($inputDate !=''){
        $date = null;
        $inputDate = date('d-m-Y',strtotime($inputDate));
        if (strlen($inputDate) > 10){
            list($date, $time) = explode(' ', $inputDate);
        }else{
            $date = $inputDate;
            $time = '00:00:00';
        }
        list($day, $month, $year) = preg_split("/[\/.-]/", $date);
        if (strlen($year)==2)
            list($year, $month, $day) = preg_split("/[\/.-]/", $date);
        
        $day = strlen($day)==1?'0'.$day:$day;
        $month = strlen($month)==1?'0'.$month:$month;
        $return_value = "$year$month$day";
    }
    return $return_value;
}
function format_tanggal_db_excel($inputDate) {
    $return_value = 'NULL';
    if ($inputDate !=''){
        $date = null;
        $inputDate = date('d/m/Y',strtotime($inputDate));
        if (strlen($inputDate) > 10){
            list($date, $time) = explode(' ', $inputDate);
        }else{
            $date = $inputDate;
            $time = '00:00:00';
        }
        list($day, $month, $year) = preg_split("/[\/.-]/", $date);
        if (strlen($year)==2)
            list($year, $month, $day) = preg_split("/[\/.-]/", $date);
        
        $day = strlen($day)==1?'0'.$day:$day;
        $month = strlen($month)==1?'0'.$month:$month;
        $return_value = "$year$month$day";
    }
    return $return_value;
}
function int2time($value){
    $hour = floor($value/60);
    $minute = $value - ($hour * 60);
    $return = (strlen($hour)==1?'0':'').$hour.':'.(strlen($minute)==1?'0':'').$minute;
    return $return;
}
function time2int($value){
    $return = $value;
    $time = explode(':',$return);
    $return = $time[0]*60+$time[1];
    return $return;
}
function terbilang($nilai) {
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    if($nilai==0){
        return "";
    }elseif ($nilai < 12&$nilai!=0) {
        return "" . $huruf[$nilai];
    } elseif ($nilai < 20) {
        return Terbilang($nilai - 10) . " Belas ";
    } elseif ($nilai < 100) {
        return Terbilang($nilai / 10) . " Puluh " . Terbilang($nilai % 10);
    } elseif ($nilai < 200) {
        return " Seratus " . Terbilang($nilai - 100);
    } elseif ($nilai < 1000) {
        return Terbilang($nilai / 100) . " Ratus " . Terbilang($nilai % 100);
    } elseif ($nilai < 2000) {
        return " Seribu " . Terbilang($nilai - 1000);
    } elseif ($nilai < 1000000) {
        return Terbilang($nilai / 1000) . " Ribu " . Terbilang($nilai % 1000);
    } elseif ($nilai < 1000000000) {
        return Terbilang($nilai / 1000000) . " Juta " . Terbilang($nilai % 1000000);
    }elseif ($nilai < 1000000000000) {
        return Terbilang($nilai / 1000000000) . " Milyar " . Terbilang($nilai % 1000000000);
    }elseif ($nilai < 100000000000000) {
        return Terbilang($nilai / 1000000000000) . " Trilyun " . Terbilang($nilai % 1000000000000);
    }elseif ($nilai <= 100000000000000) {
        return "Maaf Tidak Dapat di Proses Karena Jumlah nilai Terlalu Besar ";
    }
}
function kode_otomatis($koneksi,$param,$code,$column){
    $db = new Database();
    $db->$koneksi();
    $preff = $code;
	// $preff = $shortCode.'-'.date('y');
	$preffLen = strlen($preff)+1;
	$kode = '';
	$db->select("$param","*",NULL,NULL,NULL,"$column LIKE '%$preff%'",NULL,"$column DESC","1");
	$row = $db->getResult();
	$counter = 0;
	if (isset($row[0][$column])){
		$counter = substr($row[0][$column],$preffLen);
	}
	$counter = substr($counter + 10001,1);
    $kode = $preff.$counter;
    return $kode;
    $db->disconnect();
}
function last_id($koneksi,$param,$column){
    $db = new Database();
    $db->$koneksi();
	$db->select("$param","MAX($column) AS max_id",NULL,NULL,NULL,NULL,NULL,"$column DESC","1");
	$row = $db->getResult();
    $id = $row[0]['max_id'];
    return $id;
    $db->disconnect();
}
function summary_price($koneksi,$param,$column,$where_column,$id){
    $db = new Database();
    $db->$koneksi();
	$db->select("$param","SUM($column) AS summary",NULL,NULL,NULL,"$where_column = $id");
	$row = $db->getResult();
    $id = $row[0]['summary'];
    return $id;
    $db->disconnect();
}
function build_menu(){
    $halaman = @$_GET['halaman'];
    $result = '';
    $db = new Database();
    $db->connect_sid();
	$db->select("product_category","*",NULL,NULL,NULL,"fid_product_category = '0'");
    $row = $db->getResult();
    $active = '';
    $active_sub = '';
    foreach($row as $output){
        $id_product_category = $output['id_product_category'];
        $url = $output['url'];
        if($url == '' || $url == NULL){
            $url = 'javascript:void(0);';
        }else{
            // $url = 'javascript:void(0);';
            $url = APP_URL.'category/'.$url;
        }
        if($url == $halaman){
            $active = 'active';
        }
        $db->select("product_category","*",NULL,NULL,NULL,"fid_product_category = '$id_product_category'");
        $result .= '<li class="menu-item '.$active.'">';
        $result .= '<a class="menu-link" href="'.$url.'">'.$output['category_name'].'</a>';
        $row_sub = $db->getResult();
        $count_sub = $db->numRows();
        if($count_sub>0){
            $result .= '<ul class="sub-menu-container">';
            foreach($row_sub as $output_sub){
                $url_sub = $output_sub['url'];
                if($url_sub == '' || $url_sub == NULL){
                    $url_sub = 'javascript:void(0);';
                }else{
                    // $url_sub = APP_URL.'category/'.$output_sub['url'];
                    $url_sub = $url;
                }
                if($url_sub == $halaman){
                    $active_sub = 'active';
                }
                $result .= '<li class="menu-item '.$active_sub.'">';
                $result .= '<a class="menu-link" href="'.$url_sub.'">'.$output_sub['category_name'].'</a>';
                $result .= '</li>';
            }
            $result .= '</ul>';
        }
        $result .= '</li>';
    }
    echo $result;
    $db->disconnect();
}
function hapus_foto($koneksi,$table,$column,$column_id,$id,$direktory){
    $db = new Database();
    $db->$koneksi();
	$db->select("$table","$column",NULL,NULL,NULL,"$column_id = $id");
	$row = $db->getResult();
    $photo = $row[0][$column];
    $path = $direktory.$photo;
    // print_r($path);exit;
    unlink($direktory.$photo);
    $db->disconnect();
}
function check_stock(
    $koneksi,
    $table_item,
    $stock,
    $column_id_item,
    $id_item,
    $table_detail,
    $qty,
    $qty_detail,
    $column_fid_item,
    $token
){
$message = '';
$db = new Database();
$db->$koneksi();
$stocknya = 0;
$db->select("$table_item","$stock",NULL,NULL,NULL,"$column_id_item = $id_item");
$row = $db->getResult();
$stock = $row[0][$stock];
$db->clearResult();

$db->select("$table_detail","$qty_detail",NULL,NULL,NULL,"fid_customer = $token and $column_fid_item = $id_item");
$row_detail = $db->getResult();
$check_detail = $db->numRows();
$db->clearResult();
$qty_detailnya = $row_detail[0][$qty_detail];
$qty = $qty;

if($check_detail > 0){
    $stocknya = $stock - $qty_detailnya;
    $qtynya = $qty + $qty_detailnya;
    if($stocknya > 0){
        $db->update($table_detail,array(
            $qty_detail=>$qtynya
        ),"$column_fid_item = '$id_item' AND fid_customer = '$token'");
        $message = 1;
    }else{
        $message = 0;
    }
}else{
    $stocknya = $stock;
    if($stocknya > 0){
        $db->insert($table_detail,array(
            "fid_customer"=>$token,
            $column_fid_item=>$id_item,
            $qty_detail=>$qty
        ));
        $message = 1;
    }else{
        $message = 0;
    }
}
return $message;
$db->disconnect();
}
function update_stok($koneksi,$table_detail,$where,$column_fid_item,$column_qty,$table_item,$column_id_item){
$db = new Database();
$db->$koneksi();
$db->select("$table_detail","*",NULL,NULL,NULL,"$where");
$row_detail = $db->getResult();
foreach($row_detail AS $output){
    $iditem = $output[$column_fid_item];
    $quantity = $output[$column_qty];

    $db->customSql("UPDATE $table_item SET stok = stok - $quantity WHERE $column_id_item = $iditem");
}
$db->disconnect();
}
function decrease_cart(
    $koneksi,
    $id_item,
    $table_detail,
    $qty,
    $qty_detail,
    $column_fid_item,
    $token
){
$message = '';
$db = new Database();
$db->$koneksi();

$db->select("$table_detail","$qty_detail",NULL,NULL,NULL,"fid_customer = $token and $column_fid_item = $id_item");
$row_detail = $db->getResult();
$check_detail = $db->numRows();
$db->clearResult();
$qty_detailnya = $row_detail[0][$qty_detail];
$qty = $qty;

if($qty_detailnya > 1){
    $qtynya = $qty_detailnya-$qty;
    $db->update($table_detail,array(
        $qty_detail=>$qtynya
    ),"$column_fid_item = '$id_item' AND fid_customer = '$token'");
}else{
    $db->delete($table_detail,"$column_fid_item='$id_item' AND fid_customer = '$token'");
}
$message = 1;
return $message;
$db->disconnect();
}
function update_cart(
    $koneksi,
    $table_item,
    $stock,
    $column_id_item,
    $id_item,
    $table_detail,
    $qty,
    $qty_detail,
    $column_fid_item,
    $token
){
    $message = '';
    $db = new Database();
    $db->$koneksi();
    $stocknya = 0;
    $db->select("$table_item","$stock",NULL,NULL,NULL,"$column_id_item = $id_item");
    $row = $db->getResult();
    $stock = $row[0][$stock];
    $db->clearResult();


    $db->select("$table_detail","$qty_detail",NULL,NULL,NULL,"fid_customer = $token and $column_fid_item = $id_item");
    $row_detail = $db->getResult();
    $check_detail = $db->numRows();
    $db->clearResult();
    $qty_detailnya = $row_detail[0][$qty_detail];
    $qty = $qty;

    if($qty > 0){
        $stocknya = $stock - $qty;
        if($stocknya >= 0){
            $qtynya = $qty;
            $db->update($table_detail,array(
                $qty_detail=>$qty
            ),"$column_fid_item = '$id_item' AND fid_customer = '$token'");
            $message = 1;
        }else{
            $message = 0;
        }
    }else{
        $db->delete($table_detail,"$column_fid_item='$id_item' AND fid_customer = '$token'");
        $message = 1;
    }
    return $message;
    $db->disconnect();
}
?>