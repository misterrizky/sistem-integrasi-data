<?php
// App Config
date_default_timezone_set('Asia/Jakarta');
error_reporting(0);
define('APP_PATH', __DIR__);
define('APP_URL', 'http://'.$_SERVER['SERVER_NAME'].'/sid3/');
define('APP_ASSETS', 'http://'.$_SERVER['SERVER_NAME'].'/sid3/assets/keenthemes/');
define('BASE_ASSETS', 'http://'.$_SERVER['SERVER_NAME'].'/sid3/assets/');

set_time_limit(0);
ini_set('max_execution_time', 10000000000);
ini_set('memory_limit', "90000000000000000000M");
ini_set('post_max_size', "90000000000000000000M");

// DB Config
define('SID_HOST', 'localhost');
define('SID_NAME', 'sid');
define('SID_USER', 'root');
define('SID_PASS', 'root');

// define('SIKI_HOST', 'localhost');
// define('SIKI_NAME', 'siki_asmet');
// define('SIKI_USER', 'root');
// define('SIKI_PASS', '');
$created_at = date("Y-m-d H:i:s");

$result = array();
$alert = '';
$message = '';
$total_data = '';

require_once __DIR__ . '/sid.php';
// require_once __DIR__ . '/siki.php';
require_once __DIR__ . '/function.php';
// require_once __DIR__ . '/pagination.php';

$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$explode_url = explode("/",$url);
$real_modul = $explode_url[4];
$modul = explode("?",$real_modul);

$sid = new DatabaseSid();
$sid->connect();
// $siki = new DatabaseSiki();
// $siki->connect();
if(isset($_SESSION['token'])){
	$token = decode($_SESSION['token']);
	$sid->select("auth","*",NULL,NULL,NULL,"id='$token'");
	$res_profil = $sid->getResult();
	$token_nama = $res_profil[0]['nama'];
	$token_email = $res_profil[0]['email'];
	$token_phone = $res_profil[0]['hp'];
	$arr = explode(' ', $token_nama);
	$singkatan = "";
	foreach($arr as $kata)
	{
		$singkatan .= substr($kata, 0, 1);
	}
	$nama_singkat = substr($singkatan,-4);
}
$sid->disconnect();
// $siki->disconnect();
?>