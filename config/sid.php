<?php
class DatabaseSid{
    private $sid_host = SID_HOST;
	private $sid_user = SID_USER;
	private $sid_pass = SID_PASS;
    private $sid_name = SID_NAME;
    
    private $con = false;
    private $myconn = "";
    private $result = array();
    private $myQuery = "";
    private $numResults = "";

    public function connect(){
        if(!$this->con){
            $this->myconn = new mysqli($this->sid_host,$this->sid_user,$this->sid_pass,$this->sid_name);
            if($this->myconn->connect_errno > 0){
                array_push($this->result,$this->myconn->connect_error);
                return false;
            }else{
                $this->con = true;
                return true;
            }
        }else{
            return true;
        }
    }
    public function disconnect(){
        if($this->con){
            if($this->myconn->close()){
                $this->con = false;
                return true;
            }else{
                return false;
            }
        }
    }
    public function customSql($sql){
        $query = $this->myconn->query($sql);
        $this->myQuery = $sql;
    }
    public function sql($sql){
        $query = $this->myconn->query($sql);
        $this->myQuery = $sql;
        if($query){
            $this->numResults = $query->num_rows;
            for($i = 0; $i < $this->numResults; $i++){
                $r = $query->fetch_array();
                $key = array_keys($r);
                for($x = 0; $x < count($key); $x++){
                    if(!is_int($key[$x])){
                        if($query->num_rows >= 1){
                            $this->result[$i][$key[$x]] = $r[$key[$x]];
                        }else{
                            $this->result = null;
                        }
                    }
                }
            }
            return true;
        }else{
            array_push($this->result,$this->myconn->error);
            return false;
        }
    }
    
    public function select($table, $rows = '*', $join = null, $join_1 = null, $join_2 = null, $where = null, $group = null, $order = null, $limit = null){
        $q = 'SELECT '.$rows.' FROM '.$table;
        if($join != null){
            $q .= ' LEFT JOIN '.$join;
        }
        if($join_1 != null){
            $q .= ' LEFT JOIN '.$join_1;
        }
        if($join_2 != null){
            $q .= ' LEFT JOIN '.$join_2;
        }
        if($where != null){
            $q .= ' WHERE '.$where;
        }
        if($group != null){
            $q .= ' GROUP BY '.$group;
        }
        if($order != null){
            $q .= ' ORDER BY '.$order;
        }
        if($limit != null){
            $q .= ' LIMIT '.$limit;
        }

        $this->myQuery = $q;
        if($this->tableExists($table)){
            $query = $this->myconn->query($q);
            if($query){
                $this->numResults = $query->num_rows;
                for($i = 0; $i < $this->numResults; $i++){
                    $r = $query->fetch_array();
                    $key = array_keys($r);
                    for($x = 0; $x < count($key); $x++){
                        if(!is_int($key[$x])){
                            if($query->num_rows >= 1){
                                $this->result[$i][$key[$x]] = $r[$key[$x]];
                            }else{
                                $this->result[$i][$key[$x]] = null;
                            }
                        }
                    }
                }
                return true;
            }else{
                array_push($this->result,$this->myconn->error);
                return false;
            }
        }else{
            return false;
        }
    }

    public function insert($table,$params=array()){
        if($this->tableExists($table)){
            $sql='INSERT INTO `'.$table.'` (`'.implode('`, `',array_keys($params)).'`) VALUES ("' . implode('", "', $params) . '")';
            $this->myQuery = $sql;
            if($ins = $this->myconn->query($sql)){
                array_push($this->result,$this->myconn->insert_id);
                return true;
            }else{
                array_push($this->result,$this->myconn->error);
                return false;
            }
        }else{
            return false;
        }
    }

    public function delete($table,$where = null){
        if($this->tableExists($table)){
            if($where == null){
                $delete = 'DROP TABLE '.$table;
            }else{
                $delete = 'DELETE FROM '.$table.' WHERE '.$where;
            }

            if($del = $this->myconn->query($delete)){
                array_push($this->result,$this->myconn->affected_rows);
                $this->myQuery = $delete;
                return true;
            }else{
                array_push($this->result,$this->myconn->error);
                return false;
            }
        }else{
            return false;
        }
    }

    public function update($table,$params=array(),$where){
        if($this->tableExists($table)){
            $args=array();
            foreach($params as $field=>$value){
                $args[]=$field.'="'.$value.'"';
            }
            $sql='UPDATE '.$table.' SET '.implode(',',$args).' WHERE '.$where;
            $this->myQuery = $sql;
            if($query = $this->myconn->query($sql)){
                array_push($this->result,$this->myconn->affected_rows);
                return true;
            }else{
                array_push($this->result,$this->myconn->error);
                return false;
            }
        }else{
            return false;
        }
    }

    private function tableExists($table){
        $tablesInDb = $this->myconn->query('SHOW TABLES FROM '.$this->sid_name.' LIKE "'.$table.'"');
        if($tablesInDb){
            if($tablesInDb->num_rows == 1){
                return true;
            }else{
                array_push($this->result,$table." does not exist in this database");
                return false;
            }
        }
    }

    public function getResult(){
        $val = $this->result;
        $this->result = array();
        return $val;
    }

    public function getSql(){
        $val = $this->myQuery;
        $this->myQuery = array();
        return $val;
    }

    public function numRows(){
        $val = $this->numResults;
        $this->numResults = array();
        return $val;
    }

    public function escapeString($data){
        return $this->myconn->real_escape_string($data);
    }

    public function clearResult(){
        $this->result = array();
    }
}
?>