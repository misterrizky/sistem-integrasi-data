<?php


class Paginator {
    private $_conn;
    private $_limit;
    private $_page;
    private $_query;
    private $_total;

    public function __construct( $conn, $query ) {
     
        $this->_conn = $conn;
        $this->_query = $query;
     
        $rs= $this->_conn->query( $this->_query );
        $this->_total = $rs->num_rows;
         
    }

    public function getData( $limit, $page ) {
     
        $this->_limit   = $limit;
        $this->_page    = $page;
     
        if ( $this->_limit == 'all' ) {
            $query      = $this->_query;
        } else {
            $query      = $this->_query . " LIMIT " . ( ( $this->_page - 1 ) * $this->_limit ) . ", $this->_limit";
        }
        $rs             = $this->_conn->query( $query );
     
        while ( $row = $rs->fetch_assoc() ) {
            $results[]  = $row;
        }
     
        $result         = new stdClass();
        $result->page   = $this->_page;
        $result->limit  = $this->_limit;
        $result->total  = $this->_total;
        $result->data   = $results;
     
        return $result;
    }

    public function createLinks( $links, $limit, $page) {
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $explode_url = explode("/",$url);
        $real_modul = $explode_url[4];
        $modul = explode("?",$real_modul);
        
        $last       = ceil( $this->_total / $this->_limit );
        
        $start      = ( ( $page - $links ) > 0 ) ? $page - $links : 1;
        $end        = ( ( $page + $links ) < $last ) ? $page + $links : $last;
        
        $html       = '<div class="d-flex flex-wrap py-2 mr-3">';
        
        $class      = ( $page == 1 ) ? "disabled" : "";
        $html       .= '<a href="javascript:;" halaman="'.$modul.'?limit='.$limit.'&page=' . ( $page - 1 ) . '" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 paginasi ' . $class . '"><i class="ki ki-bold-arrow-back icon-xs"></i></a>';
        
        if ( $start > 1 ) {
            $html   .= '<a href="javascript:;" halaman="'.$modul.'?limit='.$limit.'&page=1" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1 paginasi">1</a>';
            $html   .= '<a href="javascript:;" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1 disabled">...</a>';
        }
        
        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $page == $i ) ? "active" : "";
            $html   .= '<a href="javascript:;" halaman="'.$modul.'?limit='.$limit.'&page=' . $i . '" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1 paginasi ' . $class . '">' . $i . '</a>';
        }
        
        if ( $end < $last ) {
            $html   .= '<a href="javascript:;" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">...</a>';
            $html   .= '<a href="javascript:;" halaman="'.$modul.'?limit='.$limit.'&page=' . $last . '" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1 paginasi">' . $last . '</a>';
        }
        
        $class      = ( $page == $last ) ? "disabled" : "";
        $html       .= '<a href="javascript:;" halaman="'.$modul.'?limit='.$limit.'&page=' . ( $page + 1 ) . '" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 paginasi ' . $class . '"><i class="ki ki-bold-arrow-next icon-xs"></i></a>';
        $html       .= '</div>';
        
        return $html;
    }
}