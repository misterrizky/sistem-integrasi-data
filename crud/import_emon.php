<?php

header('Content-type: text/html; charset=utf-8');
header("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
// set_time_limit(0);
// ob_implicit_flush(1);
session_start();
require_once __DIR__ . '../../config/app.php';
require_once __DIR__ . '../../config/excel_reader2.php';
$sid = new DatabaseSid();
if(isset($_SESSION['emon_file_name'])){
    $sid->connect();
    $tabel = $sid->escapeString($_POST['tabel']);
    $format = $sid->escapeString($_POST['format']);
    $propinsi = $sid->escapeString($_POST['propinsi']);
    $ta = $sid->escapeString($_POST['ta']);
    $target = basename($_SESSION['emon_file_name']);
    $path = '../assets/file/emon/' . $_SESSION['emon_file_name'];
    // if(!is_readable($path)) {
    //     $this->error = 1;
    // }
    $data = new Spreadsheet_Excel_Reader($path);
    //    menghitung jumlah baris file xls
    $baris = $data->rowcount($sheet_index=0);
    for ($i=2; $i<=$baris; $i++)
    {
        //   membaca data (kolom ke-1 sd terakhir)
        if($format == "b"){
            // FORMAT BARU
            $kd	= $data->val($i, 1);
            $kd	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $kd);

            $stkr	= $data->val($i, 2);
            $stkr	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $stkr);
            
            $ktgr	= $data->val($i, 3);
            $ktgr	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $ktgr);

            $jk	= $data->val($i, 4);
            $jenis_kontrak	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $jk);
            
            $rpl	= $data->val($i, 5);
            $rencana_pengumuman_lelang	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $rpl);
            
            $rpm	= $data->val($i, 6);
            $rpm	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $rpm);
            
            $sbsn	= $data->val($i, 7);
            $sbsn	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $sbsn);
            
            $phln	= $data->val($i, 8);
            $phln	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $phln);
            
            $barang	= $data->val($i, 9);
            $barang	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $barang);
            
            $modal	= $data->val($i, 10);
            $modal	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $modal);
            
            $dipa	= $data->val($i, 11);
            $dipa	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $dipa);
            
            $pengadaan	= $data->val($i, 12);
            $pengadaan	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $pengadaan);
            
            $vu	= $data->val($i, 13);
            $vu	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $vu);
            
            $ct	= $data->val($i, 14);
            $check_tayang	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $ct);
            
            $sirup	= $data->val($i, 15);
            $sirup	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $sirup);
            
            $tsipbj	= $data->val($i, 16);
            $tanggal_sipbj	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $tsipbj);
            
            $ssipbj	= $data->val($i, 17);
            $status_sipbj	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $ssipbj);
            
            $spse	= $data->val($i, 18);
            $kode_spse	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $spse);
            
            $tpl	= $data->val($i, 19);
            $tanggal_pengumuman_lelang	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $tpl);
            
            $tpp	= $data->val($i, 20);
            $tanggal_penetapan_pemenang	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $tpp);
            
            $trk	= $data->val($i, 21);
            $tanggal_rencana_kontrak	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $trk);
            
            $st	= $data->val($i, 22);
            $status	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $st);

            $sid->insert($tabel,array(
                'provinsi'=> $propinsi,
                'kode'=>$kd,
                'satker'=>$stkr,
                'kategori'=>$ktgr,
                'jenis_kontrak'=>$jenis_kontrak,
                'rencana_pengumuman_lelang'=>format_tanggal_db_excel($rencana_pengumuman_lelang),
                'rpm'=>num2text($rpm),
                'sbsn'=>num2text($sbsn),
                'phln'=>num2text($phln),
                'barang'=>num2text($barang),
                'modal'=>num2text($modal),
                'dipa'=>num2text($dipa),
                'pengadaan'=>num2text($pengadaan),
                'unor'=>$vu,
                'check_tayang'=>format_tanggal_db_excel($check_tayang),
                'sirup'=>$sirup,
                'tanggal_sipbj'=>format_tanggal_db_excel($tanggal_sipbj),
                'status_sipbj'=>$status_sipbj,
                'kode_spse'=>$kode_spse,
                'tanggal_pengumuman_lelang'=>format_tanggal_db_excel($tanggal_pengumuman_lelang),
                'tanggal_penetapan_pemenang'=>format_tanggal_db_excel($tanggal_penetapan_pemenang),
                'tanggal_rencana_kontrak'=>format_tanggal_db_excel($tanggal_rencana_kontrak),
                'status'=>$status,
                'ta'=>$ta,
            ));
        }else{
            // FORMAT LAMA
            $kd	= $data->val($i, 1);
            $kode	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $kd);

            $stkr	= $data->val($i, 2);
            $satker	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $stkr);
            
            $prov	= $data->val($i, 3);
            $provinsi	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $prov);

            $jk	= $data->val($i, 4);
            $jenis_kontrak	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $jk);

            $ktgr	= $data->val($i, 5);
            $kategori	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $ktgr);
            
            $nk	= $data->val($i, 6);
            $nomor_kontrak	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $nk);
            
            $rknn	= $data->val($i, 7);
            $rekanan	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $rknn);
            
            $np	= $data->val($i, 8);
            $npwp	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $np);
            
            $almt	= $data->val($i, 9);
            $alamat	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $almt);
            
            $kntrk	= $data->val($i, 10);
            $kontrak	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $kntrk);
            
            $sls	= $data->val($i, 11);
            $selesai	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $sls);
            
            $prpm	= $data->val($i, 12);
            $pagu_rpm	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $prpm);
            
            $ppln	= $data->val($i, 13);
            $pagu_pln	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $ppln);
            
            $pttl	= $data->val($i, 14);
            $pagu_total	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $pttl);
            
            $nrpm	= $data->val($i, 15);
            $nkon_rpm	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $nrpm);
            
            $npln	= $data->val($i, 16);
            $nkon_pln	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $npln);
            
            $nttl	= $data->val($i, 17);
            $nkon_total	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $nttl);
            
            $unor	= $data->val($i, 18);
            $satminkal	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $unor);
            
            $thnangg	= $data->val($i, 19);
            $ta	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $thnangg);
            $sid->insert($tabel,array(
                'kode'=>$kode,
                'satker'=>$satker,
                'provinsi'=> $provinsi,
                'jenis_kontrak'=>$jenis_kontrak,
                'kategori'=>$kategori,
                'nomor_kontrak'=>$nomor_kontrak,
                'rekanan'=>$rekanan,
                'npwp'=>$npwp,
                'alamat'=>$alamat,
                'kontrak'=>$kontrak,
                'selesai'=>$selesai,
                'pagu_rpm'=>$pagu_rpm,
                'pagu_pln'=>$pagu_pln,
                'pagu_total'=>$pagu_total,
                'nkon_rpm'=>$nkon_rpm,
                'nkon_pln'=>$nkon_pln,
                'nkon_total'=>$nkon_total,
                'satminkal'=>$satminkal,
                'ta'=>$ta,
            ));
        }
        // sleep(1);

		// if(ob_get_level() > 0)
		// {
		// 	ob_end_flush();
		// }
    }
    unlink($target);
    unset($_SESSION['emon_file_name']);
    $sid->disconnect();
}
if(isset($_SESSION['emon_file_lagi'])){
    $sid->connect();
    $tabel = $sid->escapeString($_POST['tabel']);
    $format = $sid->escapeString($_POST['format']);
    $propinsi = $sid->escapeString($_POST['propinsi']);
    $ta = $sid->escapeString($_POST['ta']);
    $target = basename($_SESSION['emon_file_lagi']);
    $path = '../assets/file/emon/' . $_SESSION['emon_file_lagi'];
    // if(!is_readable($path)) {
    //     $this->error = 1;
    // }
    $data = new Spreadsheet_Excel_Reader($path);
    //    menghitung jumlah baris file xls
    $baris = $data->rowcount($sheet_index=0);
    for ($i=2; $i<=$baris; $i++)
    {
        // FORMAT BARU
        $kd	= $data->val($i, 1);
        $kd	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $kd);

        $stkr	= $data->val($i, 2);
        $stkr	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $stkr);
        
        $ktgr	= $data->val($i, 3);
        $ktgr	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $ktgr);

        $jk	= $data->val($i, 4);
        $jenis_kontrak	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $jk);
        
        $rpl	= $data->val($i, 5);
        $rencana_pengumuman_lelang	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $rpl);
        
        $rpm	= $data->val($i, 6);
        $rpm	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $rpm);
        
        $sbsn	= $data->val($i, 7);
        $sbsn	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $sbsn);
        
        $phln	= $data->val($i, 8);
        $phln	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $phln);
        
        $barang	= $data->val($i, 9);
        $barang	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $barang);
        
        $modal	= $data->val($i, 10);
        $modal	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $modal);
        
        $dipa	= $data->val($i, 11);
        $dipa	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $dipa);
        
        $pengadaan	= $data->val($i, 12);
        $pengadaan	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $pengadaan);
        
        $vu	= $data->val($i, 13);
        $vu	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $vu);
        
        $ct	= $data->val($i, 14);
        $check_tayang	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $ct);
        
        $sirup	= $data->val($i, 15);
        $sirup	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $sirup);
        
        $tsipbj	= $data->val($i, 16);
        $tanggal_sipbj	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $tsipbj);
        
        $ssipbj	= $data->val($i, 17);
        $status_sipbj	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $ssipbj);
        
        $spse	= $data->val($i, 18);
        $kode_spse	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $spse);
        
        $tpl	= $data->val($i, 19);
        $tanggal_pengumuman_lelang	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $tpl);
        
        $tpp	= $data->val($i, 20);
        $tanggal_penetapan_pemenang	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $tpp);
        
        $trk	= $data->val($i, 21);
        $tanggal_rencana_kontrak	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $trk);
        
        $st	= $data->val($i, 22);
        $status	= preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $st);

        $sid->insert($tabel,array(
            'provinsi'=> $propinsi,
            'kode'=>$kd,
            'satker'=>$stkr,
            'kategori'=>$ktgr,
            'jenis_kontrak'=>$jenis_kontrak,
            'rencana_pengumuman_lelang'=>format_tanggal_db_excel($rencana_pengumuman_lelang),
            'rpm'=>num2text($rpm),
            'sbsn'=>num2text($sbsn),
            'phln'=>num2text($phln),
            'barang'=>num2text($barang),
            'modal'=>num2text($modal),
            'dipa'=>num2text($dipa),
            'pengadaan'=>num2text($pengadaan),
            'unor'=>$vu,
            'check_tayang'=>format_tanggal_db_excel($check_tayang),
            'sirup'=>$sirup,
            'tanggal_sipbj'=>format_tanggal_db_excel($tanggal_sipbj),
            'status_sipbj'=>$status_sipbj,
            'kode_spse'=>$kode_spse,
            'tanggal_pengumuman_lelang'=>format_tanggal_db_excel($tanggal_pengumuman_lelang),
            'tanggal_penetapan_pemenang'=>format_tanggal_db_excel($tanggal_penetapan_pemenang),
            'tanggal_rencana_kontrak'=>format_tanggal_db_excel($tanggal_rencana_kontrak),
            'status'=>$status,
            'ta'=>$ta,
        ));
        // sleep(1);

		// if(ob_get_level() > 0)
		// {
		// 	ob_end_flush();
		// }
    }
    unlink($target);
    unset($_SESSION['emon_file_lagi']);
    $sid->disconnect();
}