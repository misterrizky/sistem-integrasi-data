<?php

session_start();
require_once __DIR__ . '../../config/app.php';
$sid = new DatabaseSid();

$sid->connect();
$param = $sid->escapeString($_POST['param']);

if($param == "emon"){
    $tabel = $sid->escapeString($_POST['id']);
	$sid->select("list_emon","*",NULL,NULL,NULL,"tabel='$tabel'");
	$emon = $sid->getResult();

    $format = $emon[0]['format'];
	if($format == "l"){
		// CLEANSING 1
		$sid->sql("
		CREATE VIEW v_".$tabel."_f AS
		SELECT
		id,
		kode,
		satker,
		provinsi,
		jenis_kontrak,
		kategori,
		nomor_kontrak,
		rekanan,
		npwp,
		alamat,
		kontrak,
		selesai,
		pagu_rpm,
		pagu_pln,
		pagu_total,
		nkon_rpm,
		nkon_pln,
		nkon_total,
		ta,
		satminkal,
		DATEDIFF(selesai, kontrak) AS lama_kontrak
		FROM
		".$tabel."
		WHERE
		kategori LIKE '%Konsultan%'
		OR
		kategori LIKE '%Konstruks%'
		");
		$sid->clearResult();

		// CLEANSING 2
		$sid->sql("
		CREATE VIEW v_".$tabel."_fa AS
		SELECT
		id,
		kode,
		satker,
		provinsi,
		jenis_kontrak,
		kategori,
		nomor_kontrak,
		rekanan,
		npwp,
		Substring(npwp,14,3) as npwp_lokasi,
		alamat,
		kontrak,
		selesai,
		pagu_rpm,
		pagu_pln,
		(pagu_total*1000) AS pagu_total,
		nkon_rpm,
		nkon_pln,
		(nkon_total*1000) AS nkon_total,
		ta,
		satminkal,
		lama_kontrak
		FROM
		v_".$tabel."_f
		WHERE
		jenis_kontrak NOT LIKE '%Lanjut%'
		");
		$sid->clearResult();

		// CLEANSING 3
		$sid->sql("
		CREATE VIEW v_".$tabel."_fb AS
		SELECT
		id,
		kode,
		satker,
		provinsi,
		jenis_kontrak,
		kategori,
		nomor_kontrak,
		rekanan,
		npwp,
		alamat,
		kontrak,
		selesai,
		pagu_rpm,
		pagu_pln,
		pagu_total,
		nkon_rpm,
		nkon_pln,
		nkon_total,
		ta,
		satminkal,
		lama_kontrak,
		f_indikasi_bu(v_".$tabel."_fa.npwp) AS status_BUMN_npwp,
		f_indikasi_bu1(v_".$tabel."_fa.rekanan) AS status_BUMN_nama,
		f_indikasi_jo(v_".$tabel."_fa.satker) AS status_JO,
		f_validasi(v_".$tabel."_fa.rekanan) AS validitas,
		f_indikasi_myclanjut(v_".$tabel."_fa.lama_kontrak) AS indikasi_myc,
		f_cluster_paket_konsultan(v_".$tabel."_fa.nkon_total) AS cluster_pemaketan_konsultan,
		f_cluster_paket_kontraktor(v_".$tabel."_fa.nkon_total) AS cluster_pemaketan_kontraktor,
		f_npwp_lokasi(v_".$tabel."_fa.npwp_lokasi) AS lokasi_rekanan
		FROM
		v_".$tabel."_fa
		WHERE
		v_".$tabel."_fa.rekanan NOT LIKE ''
		AND
		v_".$tabel."_fa.rekanan NOT LIKE '-%'
		AND
		v_".$tabel."_fa.rekanan NOT LIKE '.%'
		AND
		v_".$tabel."_fa.rekanan NOT LIKE '0%'
		AND
		v_".$tabel."_fa.rekanan NOT LIKE '1%'
		AND
		v_".$tabel."_fa.rekanan NOT LIKE '2%'
		AND
		v_".$tabel."_fa.rekanan NOT LIKE '4%'
		AND
		v_".$tabel."_fa.rekanan NOT LIKE 'aa%'
		ORDER BY
		v_".$tabel."_fa.rekanan ASC
		");
		$sid->clearResult();

		// CLEANSING 4
		$sid->sql("
		CREATE VIEW v_".$tabel."_fc AS
		SELECT
		*
		FROM
		v_".$tabel."_fb
		WHERE
		v_".$tabel."_fb.nkon_total >'50000000'
		");
		$sid->clearResult();
		
		// CLEANSING 5
		$sid->sql("
		CREATE VIEW v_".$tabel."_fd AS
		SELECT
		*
		FROM
		v_".$tabel."_fc
		WHERE
		v_".$tabel."_fc.npwp != '00.000.000.0-000.000'
		AND
		v_".$tabel."_fc.npwp != '-'
		AND
		v_".$tabel."_fc.npwp != ''
		AND
		v_".$tabel."_fc.npwp != '0'
		AND 
		v_".$tabel."_fc.npwp NOT LIKE '%e%'
		ORDER BY
		v_".$tabel."_fc.npwp ASC
		");
		$sid->clearResult();
		
		// CLEANSING 6
		$sid->sql("
		CREATE VIEW v_".$tabel."_fd_tahap1 AS
		SELECT
		*,
		IF(status_BUMN_nama = 'BUMN' AND status_BUMN_npwp = 'BUMN','BUMN' , 0) AS BUMN,
		f_pulau(v_".$tabel."_fc.provinsi) AS pulau,
		f_indikasi_bu2(v_".$tabel."_fc.rekanan) AS BUMN_1,
		IF(v_".$tabel."_fc.lokasi_rekanan = v_".$tabel."_fc.provinsi,'LOKAL' , 0) AS STATUS_LOKAL,
		f_cluster_bmk(v_".$tabel."_fc.nkon_total) AS bmk,
		f_indikasi_bumn(v_".$tabel."_fc.rekanan) AS nama_bumn
		FROM
		v_".$tabel."_fc
		WHERE
		v_".$tabel."_fc.npwp != '00.000.000.0-000.000'
		AND
		v_".$tabel."_fc.npwp != '-'
		AND
		v_".$tabel."_fc.npwp != ''
		AND
		v_".$tabel."_fc.npwp != '0'
		AND 
		v_".$tabel."_fc.npwp NOT LIKE '%e%'
		ORDER BY
		v_".$tabel."_fc.npwp ASC
		");
		$sid->clearResult();

		// CLEANSING 7
		$sid->sql("
		CREATE VIEW v_".$tabel."_fd_tahap1a AS
		SELECT
		*,
		CASE v_".$tabel."_fd_tahap1.nama_bumn
		WHEN '-'  THEN v_".$tabel."_fd_tahap1.rekanan
		ELSE v_".$tabel."_fd_tahap1.nama_bumn END AS rekanan1
		FROM
		v_".$tabel."_fd_tahap1
		");
		$sid->clearResult();

		// CLEANSING 8
		$sid->sql("
		CREATE VIEW v_".$tabel."_fe AS
		SELECT
		*
		FROM
		v_".$tabel."_fd
		WHERE
		v_".$tabel."_fd.indikasi_myc = 'Tahunan'
		");
		$sid->clearResult();

		// CLEANSING 9
		$sid->sql("
		CREATE VIEW v_".$tabel."_ff AS
		SELECT
		*
		FROM
		v_".$tabel."_fe
		WHERE
		v_".$tabel."_fe.lokasi_rekanan NOT LIKE '-'
		");
		$sid->clearResult();
	}else{
		// CLEANSING 1
		$sid->sql("
		CREATE VIEW v_".$tabel."_f AS
		SELECT
		*,
		DATEDIFF(tanggal_rencana_kontrak, tanggal_pengumuman_lelang) AS lama_kontrak
		FROM
		".$tabel."
		WHERE
		kategori LIKE '%Konsultan%'
		OR
		kategori LIKE '%Konstruks%'
		");
		$sid->clearResult();
	}

    $sid->update('list_emon',array(
        'st'=>1,
        'cleansing_at'=>$created_at,
        'cleansing_by'=>$token_nama
    ),"tabel = '$tabel'");

    $alert = 'success';
    $message = 'File berhasil di cleansing';
    
    $result['alert'] = $alert;
	$result['message'] = $message;
    echo json_encode($result);
}

$sid->disconnect();