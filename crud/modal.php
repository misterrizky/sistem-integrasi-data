<?php

session_start();
require_once __DIR__ . '../../config/app.php';
$sid = new DatabaseSid();

$sid->connect();
$modal = $sid->escapeString($_POST['modal']);

if($modal == "upload_emon"){
    $id = decode($sid->escapeString($_POST['id']));

    $sid->select("list_emon","*",NULL,NULL,NULL,"id ='$id'");
    $data = $sid->getResult();
    $tanggal = str_replace("-","_",$data[0]['tanggal']);
    $tabel = "emon_" . $tanggal;
    ?>
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Upload e-Mon <?=$data[0]['nama'];?></h5>
        <div data-bs-dismiss="modal" class="btn btn-icon btn-sm btn-active-icon-primary">
            <span class="svg-icon svg-icon-1">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                        <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                        <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                    </g>
                </svg>
            </span>
        </div>
    </div>
    <div class="modal-body">
        <input type="hidden" class="form-control" name="id" value="<?=$data[0]['id'];?>" />
        <input type="hidden" class="form-control" name="tabel" value="<?=$tabel;?>" />
        <div class="form-group row" id="format_baru_emon" style="display:none;">
            <div class="col-lg-6">
                <label>Propinsi:</label>
                <select class="form-control" id="propinsi_emon" name="propinsi">
                    <option selected disabled>Pilih Propinsi</option>
                    <?
                    $sid->select("propinsi","*");
                    $prop = $sid->getResult();
                    foreach($prop AS $list){
                    ?>
                    <option value="<?=$list['name'];?>"><?=$list['name'];?></option>
                    <?
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-6">
                <label>Tahun Anggaran:</label>
                <input type="text" class="form-control" id="ta_emon" name="ta" maxlength="4">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-12">
                <label>Format:</label>
                <select class="form-control" id="format_emon" name="format">
                    <option selected disabled>Pilih format file</option>
                    <option value="b">Baru</option>
                    <option value="l">Lama</option>
                </select>
            </div>
            <div class="col-lg-12">
                <label>File</label>
                <input type="file" class="form-control" name="file_emon" id="file_emon" accept=".xls" required />
            </div>
        </div>
        <div class="form-group" id="process_emon" style="display:none;">
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <span id="process_data_emon">0</span> - <span id="total_data_emon">0</span>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
        <button type="submit" id="upload_emon" name="upload_emon" onclick="handle_upload_modal('upload_emon','#form_upload_emon','Upload','#process_emon','#process_data_emon','#total_data_emon');" type="button" class="btn btn-primary font-weight-bold">Upload</button>
    </div>
    <script type="text/javascript">
    number_only('ta_emon');
    obj_yearpicker('#ta_emon');
    $("#format_emon").change(function(){
        let format = $("#format_emon").val();
        if(format == "b"){
            $("#format_baru_emon").show();
        }else{
            $("#format_baru_emon").hide();
        }
    });
    </script>
    <?php
}
$sid->disconnect();