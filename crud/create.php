<?php
session_start();
require_once __DIR__ . '../../config/app.php';
$sid = new DatabaseSid();

if(isset($_POST['tambah_emon'])){
    $sid->connect();
    $nama = $sid->escapeString($_POST['nama']);
    $tanggal = format_tanggal_db($sid->escapeString($_POST['tanggal']));

	if(!$nama OR !$tanggal OR !$format){
		$alert = 'info';
        $message = 'Harap masukkan semua kolom';
	}

    $sid->select("list_emon","*",NULL,NULL,NULL,"nama = '$nama' AND tanggal = '$tanggal' ");
    $count = $sid->numRows();
    if($count > 0){
        $alert = 'info';
        $message = 'e-Mon dengan '.$nama.' pada tanggal ' .tanggal($tanggal). ' sudah tersedia';
    }else{
        $sid->insert('list_emon',array(
            'nama'=>$nama,
            'tanggal'=>$tanggal,
            'format'=>'-',
            'st'=> 0,
            'created_by'=>$token_nama,
            'created_at'=>$created_at,
        ));
        $alert = 'success';
        $message = 'e-Mon dengan '.$nama.' pada tanggal ' .tanggal($tanggal). ' berhasil tersimpan';
    }
    $result['alert'] = $alert;
	$result['message'] = $message;
	echo json_encode($result);
    $sid->disconnect();
}

if(isset($_POST['tambah_pengguna'])){
    $sid->connect();
    $nama = $sid->escapeString($_POST['nama']);
    $email = $sid->escapeString($_POST['email']);
    $username = $sid->escapeString($_POST['username']);
    $password = $sid->escapeString($_POST['password']);
    $pass = encrypt_decrypt("encrypt",$password);
	if(!$username OR !$password OR !$nama OR !$email){
        $alert = 'warning';
        $message .= 'Harap masukkan semua kolom';
    }
    if (!$message){
        $sid->select("auth","*",NULL,NULL,NULL,"username='$username' OR email ='$email'");
		$respon_login = $sid->getResult();
		$count_login = $sid->numRows();
		if($count_login>0){
            $alert = 'danger';
			$message = "Pengguna sudah tersedia";
		}else{
            $sid->insert('auth',array(
                'nama'=>$nama,
                'username'=>$username,
                'email'=>$email,
                'foto'=>'0',
                'hp'=>'0',
                'alamat'=>'-',
                'akses'=>'1',
                'direktorat'=>'0',
                'created_by'=>'0',
                'created_date'=>date("Y-m-d H:i:s"),
                'password'=>$pass
            ));
            $alert = 'success';
            $message = 'Pengguna tersimpan';
		}
    }
    $result['alert'] = $alert;
	$result['message'] = $message;
	echo json_encode($result);
    $sid->disconnect();
}