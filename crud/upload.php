<?php

session_start();
require_once __DIR__ . '../../config/app.php';
require_once __DIR__ . '../../config/excel_reader2.php';
$sid = new DatabaseSid();

if(isset($_POST['upload_emon'])){
    $sid->connect();
    unset($_SESSION['emon_file_name']);
    $id = $sid->escapeString($_POST['id']);
    $sid->select("list_emon","*",NULL,NULL,NULL,"id = '$id'");
    $respons = $sid->getResult();
    $format = $sid->escapeString($_POST['format']);
    $tabel = $sid->escapeString($_POST['tabel']);
    $propinsi = $sid->escapeString($_POST['propinsi']);
    $ta = $sid->escapeString($_POST['ta']);
    if($format == "b"){
        if(!$propinsi OR !$ta){
            $alert = 'info';
            $message = 'Harap masukkan semua kolom';
        }
    }

    $file =strtolower($_FILES['file_emon']['name']);
	$file_loc = $_FILES['file_emon']['tmp_name'];
	$file_size = $_FILES['file_emon']['size'];
	$file_type = strtolower($_FILES['file_emon']['type']);
    $new_file_name = $tabel.".xls";
	$path = "../assets/file/emon/".$new_file_name;
	copy($_FILES["file_emon"]["tmp_name"],$path);

    if(!$_FILES['file_emon']['name'] OR !$format){
        $alert = 'info';
        $message = 'Harap masukkan semua kolom';
    }
    if($_FILES['file_emon']['name']){
        $target = basename($_FILES['file_emon']['name']);
        // $_SESSION['emon_file_name'] = $_FILES['file_emon']['name'];
        $_SESSION['emon_file_name'] = $new_file_name;
        move_uploaded_file($_FILES['file_emon']['tmp_name'], $target);

        $counter = new Spreadsheet_Excel_Reader($file);
        $total_line = $counter->rowcount($sheet_index=0);
        if($format == "b"){
            $sid->sql("CREATE TABLE $tabel LIKE emon_master_new");
        }else{
            $sid->sql("CREATE TABLE $tabel LIKE emon_master");
        }
        $sid->clearResult();
        $sid->update('list_emon',array(
            'tabel'=>$tabel,
            'format'=>$format,
            'uploaded_at'=>$created_at,
            'uploaded_by'=>$token_nama
        ),"id = '$id'");
        $sid->clearResult();
        $sid->insert("log_notif",array(
            'tipe'=>1,
            'st'=>0,
            'tabel'=> $tabel,
            'created_at'=>$created_at,
            'created_by'=>$token_nama,
        ));
        $sid->clearResult();
        
        $alert = 'success';
        $message = 'File berhasil di upload';
        $total_data = $total_line-1;
    }

    $result['alert'] = $alert;
	$result['message'] = $message;
	$result['total_data'] = $total_data;
	echo json_encode($result);
    $sid->disconnect();
}
if(isset($_POST['upload_emon_lagi'])){
    $sid->connect();
    unset($_SESSION['emon_file_name']);
    $id = $sid->escapeString($_POST['id']);
    $sid->select("list_emon","*",NULL,NULL,NULL,"id = '$id'");
    $respons = $sid->getResult();
    $format = $sid->escapeString($_POST['format']);
    $tabel = $sid->escapeString($_POST['tabel']);
    $propinsi = $sid->escapeString($_POST['propinsi']);
    $ta = $sid->escapeString($_POST['ta']);
    if($format == "b"){
        if(!$propinsi OR !$ta){
            $alert = 'info';
            $message = 'Harap masukkan semua kolom';
        }
    }

    $file =strtolower($_FILES['file_emon']['name']);
	$file_loc = $_FILES['file_emon']['tmp_name'];
	$file_size = $_FILES['file_emon']['size'];
	$file_type = strtolower($_FILES['file_emon']['type']);
    $new_file_name = $tabel.".xls";
	$path = "../assets/file/emon/".$new_file_name;
	copy($_FILES["file_emon"]["tmp_name"],$path);

    if(!$_FILES['file_emon']['name'] OR !$format){
        $alert = 'info';
        $message = 'Harap masukkan semua kolom';
    }
    if($_FILES['file_emon']['name']){
        $target = basename($_FILES['file_emon']['name']);
        // $_SESSION['emon_file_name'] = $_FILES['file_emon']['name'];
        $_SESSION['emon_file_lagi'] = $new_file_name;
        move_uploaded_file($_FILES['file_emon']['tmp_name'], $target);

        $counter = new Spreadsheet_Excel_Reader($file);
        $total_line = $counter->rowcount($sheet_index=0);

        $sid->clearResult();
        
        $alert = 'success';
        $message = 'File berhasil di upload';
        $total_data = $total_line-1;
    }

    $result['alert'] = $alert;
	$result['message'] = $message;
	$result['total_data'] = $total_data;
	echo json_encode($result);
    $sid->disconnect();
}