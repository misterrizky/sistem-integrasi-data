<?php
session_start();
require_once __DIR__ . '../../config/app.php';
$sid = new DatabaseSid();

if(isset($_POST['login'])){
    $sid->connect();
    $username = $sid->escapeString($_POST['username']);
    $password = $sid->escapeString($_POST['password']);
    $pass = encrypt_decrypt("encrypt",$password);
	if(!$username OR !$password){
        $alert = 'warning';
        $message .= 'Harap masukkan semua kolom';
    }
    if (!$message){
        $sid->select("auth","*",NULL,NULL,NULL,"username='$username'");
		$respon_login = $sid->getResult();
		$count_login = $sid->numRows();
		if($count_login>0){
			if($pass == $respon_login[0]["password"]){
				$token = encode($respon_login[0]['id']);
				$_SESSION['token'] = $token;
                $alert = 'success';
				$message = 'Selamat datang kembali '.$respon_login[0]['nama'];
			}else{
				$alert = 'info';
				$message = 'Kata sandi tidak benar';
			}
		}else{
            $alert = 'danger';
			$message = "Anda tidak terdaftar di SID v3.0";
		}
    }
    $result['alert'] = $alert;
	$result['message'] = $message;
	echo json_encode($result);
    $sid->disconnect();
}