<?php
require_once __DIR__ . '../../config/app.php';
$sid = new DatabaseSid();
$sid->connect();

$code = $_GET['params'];
$sid->select("list_emon","*",NULL,NULL,NULL,"tabel='$code'");
$emon = $sid->getResult();
$sid->clearResult();
$tabel = $emon[0]['tabel'];
if($emon[0]['st'] == 1){
    $sid->clearResult();
    $sid->select("v_".$tabel."_f","*");
    $count_1 = $sid->numRows();

    $sid->clearResult();
    $sid->select("v_".$tabel."_fa","*");
    $count_2 = $sid->numRows();

    $sid->clearResult();
    $sid->select("v_".$tabel."_fb","*");
    $count_3 = $sid->numRows();

    $sid->clearResult();
    $sid->select("v_".$tabel."_fc","*");
    $count_4 = $sid->numRows();

    $sid->clearResult();
    $sid->select("v_".$tabel."_fd","*");
    $count_5 = $sid->numRows();

    $sid->clearResult();
    $sid->select("v_".$tabel."_fd_tahap1","*");
    $count_6 = $sid->numRows();

    $sid->clearResult();
    $sid->select("v_".$tabel."_fd_tahap1a","*");
    $count_7 = $sid->numRows();

    $sid->clearResult();
    $sid->select("v_".$tabel."_fe","*");
    $count_8 = $sid->numRows();

    $sid->clearResult();
    $sid->select("v_".$tabel."_ff","*");
    $count_9 = $sid->numRows();
}
?>
<ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
    <li class="nav-item">
        <a class="nav-link text-active-primary active">
            List Cleansing :
        </a>
    </li>
    <li class="nav-item">
        <div class="border border-gray-300 border-dashed rounded min-w-30px py-3 px-4 me-6 mb-3">
            <div class="d-flex align-items-center">
                <div class="fs-6 text-center fw-bolder" data-kt-countup="true" data-kt-countup-value="<?=$count_1;?>" data-kt-countup-prefix="">0</div>
            </div>
            <a href="<?= APP_URL;?>emon/cleansing-1/<?=$tabel?>" target="_blank" class="fw-bold text-center fs-6 text-gray-400">1</a>
        </div>
    </li>
    <li class="nav-item">
        <div class="border border-gray-300 border-dashed rounded min-w-30px py-3 px-4 me-6 mb-3">
            <div class="d-flex align-items-center">
                <div class="fs-6 text-center fw-bolder" data-kt-countup="true" data-kt-countup-value="<?=$count_2;?>" data-kt-countup-prefix="">0</div>
            </div>
            <a href="<?= APP_URL;?>emon/cleansing-2/<?=$tabel?>" target="_blank" class="fw-bold text-center fs-6 text-gray-400">2</a>
        </div>
    </li>
    <li class="nav-item">
        <div class="border border-gray-300 border-dashed rounded min-w-30px py-3 px-4 me-6 mb-3">
            <div class="d-flex align-items-center">
                <div class="fs-6 text-center fw-bolder" data-kt-countup="true" data-kt-countup-value="<?=$count_3;?>" data-kt-countup-prefix="">0</div>
            </div>
            <a href="<?= APP_URL;?>emon/cleansing-3/<?=$tabel?>" target="_blank" class="fw-bold text-center fs-6 text-gray-400">3</a>
        </div>
    </li>
    <li class="nav-item">
        <div class="border border-gray-300 border-dashed rounded min-w-30px py-3 px-4 me-6 mb-3">
            <div class="d-flex align-items-center">
                <div class="fs-6 text-center fw-bolder" data-kt-countup="true" data-kt-countup-value="<?=$count_4;?>" data-kt-countup-prefix="">0</div>
            </div>
            <a href="<?= APP_URL;?>emon/cleansing-4/<?=$tabel?>" target="_blank" class="fw-bold text-center fs-6 text-gray-400">4</a>
        </div>
    </li>
    <li class="nav-item">
        <div class="border border-gray-300 border-dashed rounded min-w-30px py-3 px-4 me-6 mb-3">
            <div class="d-flex align-items-center">
                <div class="fs-6 text-center fw-bolder" data-kt-countup="true" data-kt-countup-value="<?=$count_5;?>" data-kt-countup-prefix="">0</div>
            </div>
            <a href="<?= APP_URL;?>emon/cleansing-5/<?=$tabel?>" target="_blank" class="fw-bold text-center fs-6 text-gray-400">5</a>
        </div>
    </li>
    <li class="nav-item">
        <div class="border border-gray-300 border-dashed rounded min-w-30px py-3 px-4 me-6 mb-3">
            <div class="d-flex align-items-center">
                <div class="fs-6 text-center fw-bolder" data-kt-countup="true" data-kt-countup-value="<?=$count_6;?>" data-kt-countup-prefix="">0</div>
            </div>
            <a href="<?= APP_URL;?>emon/cleansing-6/<?=$tabel?>" target="_blank" class="fw-bold text-center fs-6 text-gray-400">6</a>
        </div>
    </li>
    <li class="nav-item">
        <div class="border border-gray-300 border-dashed rounded min-w-30px py-3 px-4 me-6 mb-3">
            <div class="d-flex align-items-center">
                <div class="fs-6 text-center fw-bolder" data-kt-countup="true" data-kt-countup-value="<?=$count_7;?>" data-kt-countup-prefix="">0</div>
            </div>
            <a href="<?= APP_URL;?>emon/cleansing-7/<?=$tabel?>" target="_blank" class="fw-bold text-center fs-6 text-gray-400">7</a>
        </div>
    </li>
    <li class="nav-item">
        <div class="border border-gray-300 border-dashed rounded min-w-30px py-3 px-4 me-6 mb-3">
            <div class="d-flex align-items-center">
                <div class="fs-6 text-center fw-bolder" data-kt-countup="true" data-kt-countup-value="<?=$count_8;?>" data-kt-countup-prefix="">0</div>
            </div>
            <a href="<?= APP_URL;?>emon/cleansing-8/<?=$tabel?>" target="_blank" class="fw-bold text-center fs-6 text-gray-400">8</a>
        </div>
    </li>
    <li class="nav-item">
        <div class="border border-gray-300 border-dashed rounded min-w-30px py-3 px-4 me-6 mb-3">
            <div class="d-flex align-items-center">
                <div class="fs-6 text-center fw-bolder" data-kt-countup="true" data-kt-countup-value="<?=$count_9;?>" data-kt-countup-prefix="">0</div>
            </div>
            <a href="<?= APP_URL;?>emon/cleansing-9/<?=$tabel?>" target="_blank" class="fw-bold text-center fs-6 text-gray-400">9</a>
        </div>
    </li>
</ul>
<?php
$sid->disconnect();
?>