<?php
session_start();
require_once __DIR__ . '../../config/app.php';
$sid = new DatabaseSid();
$sid->connect();
$code = $_GET['code'];
$sid->select($code,"
*
");
$table_respon = $sid->getResult();

$array = array();
$no = 0;
foreach($table_respon as $list){
    $no++;
    $id = encode($list['id']);
    $provinsi = $list['provinsi'];
    $paket = $list['kode'] . '<br>' . $list['satker'];
    $kategori = $list['kategori'];
    $kontrak = $list['jenis_kontrak'];
    $rencana_pengumuman = tanggal($list['rencana_pengumuman']);
    $rpm = thousand($list['rpm']);
    $sbsn = thousand($list['sbsn']);
    $phln = thousand($list['phln']);
    $barang = thousand($list['barang']);
    $modal = thousand($list['modal']);
    $dipa = thousand($list['dipa']);
    $pengadaan = thousand($list['pengadaan']);
    $unor = $list['unor'];
    $check_tayang = tanggal($list['check_tayang']);
    $sirup = $list['sirup'];
    $tanggal_sipbj = tanggal($list['tanggal_sipbj']);
    $status_sipbj = $list['status_sipbj'];
    $kode_spse = $list['kode_spse'];
    $tanggal_pengumuman_lelang = tanggal($list['tanggal_pengumuman_lelang']);
    $tanggal_penetapan_pemenang = tanggal($list['tanggal_penetapan_pemenang']);
    $tanggal_rencana_kontrak = tanggal($list['tanggal_rencana_kontrak']);
    $status = $list['status'];
    $ta = $list['ta'];
    $temp=array(
        "no"=>$no,
        "id"=>$id,
        "provinsi"=>$provinsi,
        "paket"=>$paket,
        "kategori"=>$kategori,
        "kontrak"=>$kontrak,
        "rencana_pengumuman"=>$rencana_pengumuman,
        "rpm"=>$rpm,
        "sbsn"=>$sbsn,
        "phln"=>$phln,
        "barang"=>$barang,
        "modal"=>$modal,
        "dipa"=>$dipa,
        "pengadaan"=>$pengadaan,
        "unor"=>$unor,
        "check_tayang"=>$check_tayang,
        "sirup"=>$sirup,
        "tanggal_sipbj"=>$tanggal_sipbj,
        "status_sipbj"=>$status_sipbj,
        "kode_spse"=>$kode_spse,
        "tanggal_pengumuman_lelang"=>$tanggal_pengumuman_lelang,
        "tanggal_penetapan_pemenang"=>$tanggal_penetapan_pemenang,
        "tanggal_rencana_kontrak"=>$tanggal_rencana_kontrak,
        "status"=>$status,
        "ta"=>$ta,
	);
	array_push($array,$temp);
}
$data = json_encode($array);
echo "{\"data\" : " .$data."}";
$sid->disconnect();
?>