<?php
session_start();
require_once __DIR__ . '../../config/app.php';
$sid = new DatabaseSid();
$sid->connect();
$sid->select("list_emon","*",null,null,null,null,null,'id DESC');
$table_respon = $sid->getResult();
$status = '';
$format = '';
$array = array();
$no = 0;
foreach($table_respon as $list){
    $no++;
    $id = encode($list['id']);
    $tanggal = tanggal($list['tanggal']);
    $created_at = tanggal($list['created_at']);
    $uploaded_at = tanggal($list['uploaded_at']);
    $cleansing_at = tanggal($list['cleansing_at']);
    $created_by = $list['created_by'];
    $uploaded_by = $list['uploaded_by'];
    $cleansing_by = $list['cleansing_by'];
    $nama = $list['nama'];
    $tabel = $list['tabel'];
    if($list['st'] == 0){
        $status = 'Belum di cleansing';
    }else{
        $status = 'Sudah di cleansing';
    }
    if($list['format'] == "b"){
        $format = 'Baru';
    }elseif($list['format'] == "l"){
        $format = 'Lama';
    }else{
        $format = '-';
    }
    $temp=array(
        "no"=>$no,
        "id"=>$id,
        "tanggal"=>$tanggal,
        "created_at"=>$created_at,
        "uploaded_at"=>$uploaded_at,
        "cleansing_at"=>$cleansing_at,
        "created_by"=>$created_by,
        "uploaded_by"=>$uploaded_by,
        "cleansing_by"=>$cleansing_by,
        "nama"=>$nama,
        "format"=>$format,
        "tabel"=>$tabel,
        "st"=>$status
	);
	array_push($array,$temp);
}
$data = json_encode($array);
echo "{\"data\" : " .$data."}";
$sid->disconnect();
?>