<?php
session_start();
require_once __DIR__ . '../../config/app.php';
$sid = new DatabaseSid();
$sid->connect();
$sid->select("auth","*");
$table_respon = $sid->getResult();

$array = array();
$no = 0;
foreach($table_respon as $list){
    $no++;
    $id = encode($list['id']);
    $date = tanggal($list['created_date']);
    $username = $list['username'];
    $email = $list['email'];
    $nama = $list['nama'];
    $temp=array(
        "no"=>$no,
        "id"=>$id,
        "date"=>$date,
        "username"=>$username,
        "email"=>$email,
        "nama"=>$nama
	);
	array_push($array,$temp);
}
$data = json_encode($array);
echo "{\"data\" : " .$data."}";
$sid->disconnect();
?>