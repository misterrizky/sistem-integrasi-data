<?php
session_start();
require_once __DIR__ . '../../config/app.php';
$sid = new DatabaseSid();
$sid->connect();
$code = $_GET['table'];
// print_r($code);exit;
$sid->select("v_".$code."_fd_tahap1a","
SUM( CASE WHEN ta = '2015' THEN nkon_total ELSE 0 END ) lima_belas,
SUM( CASE WHEN ta = '2016' THEN nkon_total ELSE 0 END ) enam_belas,
SUM( CASE WHEN ta = '2017' THEN nkon_total ELSE 0 END ) tujuh_belas,
SUM( CASE WHEN ta = '2018' THEN nkon_total ELSE 0 END ) lapan_belas,
SUM( CASE WHEN ta = '2019' THEN nkon_total ELSE 0 END ) bilan_belas,
BUMN_1
",null,null,null,null,"BUMN_1");
$sql = $sid->getSql();
$table_respon = $sid->getResult();

$arr = array();
$no = 0;
foreach($table_respon as $list){
    $lima_belas = $list['lima_belas'];
	$enam_belas = $list['enam_belas'];
	$tujuh_belas = $list['tujuh_belas'];
	$lapan_belas = $list['lapan_belas'];
	$bilan_belas = $list['bilan_belas'];
	$BUMN_1 = $list['BUMN_1'];
	$temp=array(
    "lima_belas"=>"Rp. ".number_format($lima_belas),
    "enam_belas"=>"Rp. ".number_format($enam_belas),
    "tujuh_belas"=>"Rp. ".number_format($tujuh_belas),
    "lapan_belas"=>"Rp. ".number_format($lapan_belas),
    "bilan_belas"=>"Rp. ".number_format($bilan_belas),
	"BUMN_1"=>$BUMN_1
	);
   array_push($arr,$temp);
}
$data = json_encode($arr);
echo "{\"data\" : " .$data."}";
$sid->disconnect();
?>