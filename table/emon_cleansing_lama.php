<?php
session_start();
require_once __DIR__ . '../../config/app.php';
$sid = new DatabaseSid();
$sid->connect();
$code = $_GET['code'];
$cleansing = str_replace("cleansing-","",$_GET['cleansing']);
if($cleansing == 1){
    $sid->select("v_".$code."_f","*");
}elseif($cleansing == 2){
    $sid->select("v_".$code."_fa","*");
}elseif($cleansing == 3){
    $sid->select("v_".$code."_fb","*");
}elseif($cleansing == 4){
    $sid->select("v_".$code."_fc","*");
}elseif($cleansing == 5){
    $sid->select("v_".$code."_fd","*");
}elseif($cleansing == 6){
    $sid->select("v_".$code."_fd_tahap1","*");
}elseif($cleansing == 7){
    $sid->select("v_".$code."_fd_tahap1a","*");
}elseif($cleansing == 8){
    $sid->select("v_".$code."_fe","*");
}elseif($cleansing == 9){
    $sid->select("v_".$code."_ff","*");
}
$table_respon = $sid->getResult();

$array = array();
$no = 0;
foreach($table_respon as $list){
    $no++;
    $id = encode($list['id']);
    $paket = $list['kode'] . '<br>' . $list['satker'];
    $kontrak = $list['nomor_kontrak'] . '<br>' . $list['jenis_kontrak'];
    $provinsi = $list['provinsi'];
    $kategori = $list['kategori'];
    $rekanan = $list['npwp'] . '<br>' . $list['rekanan'];
    $alamat = $list['alamat'];
    $tkontrak = tanggal($list['kontrak']);
    $selesai = tanggal($list['selesai']);
    $pagu_rpm = thousand($list['pagu_rpm']);
    $pagu_pln = thousand($list['pagu_pln']);
    $pagu_total = thousand($list['pagu_total']);
    $nkon_rpm = thousand($list['nkon_rpm']);
    $nkon_pln = thousand($list['nkon_pln']);
    $nkon_total = thousand($list['nkon_total']);
    $unor = $list['satminkal'];
    $ta = $list['ta'];
    $temp=array(
        "no"=>$no,
        "id"=>$id,
        "paket"=>$paket,
        "kontrak"=>$kontrak,
        "provinsi"=>$provinsi,
        "kategori"=>$kategori,
        "rekanan"=>$rekanan,
        "alamat"=>$alamat,
        "tkontrak"=>$tkontrak,
        "selesai"=>$selesai,
        "pagu_rpm"=>$pagu_rpm,
        "pagu_pln"=>$pagu_pln,
        "pagu_total"=>$pagu_total,
        "nkon_rpm"=>$nkon_rpm,
        "nkon_pln"=>$nkon_pln,
        "nkon_total"=>$nkon_total,
        "unor"=>$unor,
        "ta"=>$ta
	);
	array_push($array,$temp);
}
$data = json_encode($array);
echo "{\"data\" : " .$data."}";
$sid->disconnect();
?>