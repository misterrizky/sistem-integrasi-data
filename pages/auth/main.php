<?php
if(!isset($_SESSION['token'])){
?>
<div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
    <!--begin::Form-->
    <form class="form w-100" novalidate="novalidate" id="login_form">
        <!--begin::Heading-->
        <div class="text-center mb-10">
            <!--begin::Title-->
            <h1 class="text-dark mb-3">SID v3.0</h1>
            <!--end::Title-->
            <!--begin::Link-->
            <!-- <div class="text-gray-400 fw-bold fs-4">New Here?
            <a href="authentication/flows/basic/sign-up.html" class="link-primary fw-bolder">Create an Account</a></div> -->
            <!--end::Link-->
        </div>
        <!--begin::Heading-->
        <!--begin::Input group-->
        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="form-label fs-6 fw-bolder text-dark">ID Pengguna</label>
            <!--end::Label-->
            <!--begin::Input-->
            <input class="form-control form-control-lg form-control-solid" maxlength="20" type="text" id="username" name="username" autocomplete="off" data-login="1" />
            <!--end::Input-->
        </div>
        <!--end::Input group-->
        <!--begin::Input group-->
        <div class="fv-row mb-10">
            <!--begin::Wrapper-->
            <div class="d-flex flex-stack mb-2">
                <!--begin::Label-->
                <label class="form-label fw-bolder text-dark fs-6 mb-0">Kata Sandi</label>
                <!--end::Label-->
                <!--begin::Link-->
                <!-- <a href="authentication/flows/basic/password-reset.html" class="link-primary fs-6 fw-bolder">Forgot Password ?</a> -->
                <!--end::Link-->
            </div>
            <!--end::Wrapper-->
            <!--begin::Input-->
            <input class="form-control form-control-lg form-control-solid" type="password" id="password" name="password" autocomplete="off" data-login="2" />
            <!--end::Input-->
        </div>
        <!--end::Input group-->
        <!--begin::Actions-->
        <div class="text-center">
            <!--begin::Submit button-->
            <button type="submit" id="tombol_login" class="btn btn-lg btn-primary w-100 mb-5">
                <span class="indicator-label">Masuk</span>
                <span class="indicator-progress">Harap Tunggu...
                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
            </button>
            <!--end::Submit button-->
        </div>
        <!--end::Actions-->
    </form>
    <!--end::Form-->
</div>
<?php
}else{
	echo "<script>window.location.href='".APP_URL."welcome'</script>";
}
?>