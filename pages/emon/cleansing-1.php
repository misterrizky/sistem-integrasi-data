<?php
if(isset($_SESSION['token'])){
    $code = $_GET['params'];
    $sid->select("list_emon","*",NULL,NULL,NULL,"tabel='$code'");
    $emon = $sid->getResult();
    $format = $emon[0]['format'];
    if($format == "b"){
        $format = "Baru";
    }else{
        $format = "Lama";
    }
?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Container-->
    <div class="container" id="kt_content_container">
        <!--begin::Navbar-->
        <div class="card mb-5 mb-xl-10">
            <div class="card-body pt-9 pb-0">
                <!--begin::Details-->
                <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                    <!--begin::Info-->
                    <div class="flex-grow-1">
                        <!--begin::Title-->
                        <div id="content_info"></div>
                        <!--end::Title-->
                    </div>
                    <!--end::Info-->
                </div>
                <!--end::Details-->
                <!--begin::Navs-->
                <div class="d-flex overflow-auto h-55px">
                    <div id="content_counter"></div>
                </div>
                <!--begin::Navs-->
            </div>
        </div>
        <!--end::Navbar-->
        <!--begin::details View-->
        <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
            <!--begin::Card header-->
            <div class="card-header cursor-pointer">
                <!--begin::Card title-->
                <div class="card-title m-0">
                    <h3 class="fw-bolder m-0">List Emon Cleansing 1</h3>
                </div>
                <!--end::Card title-->
            </div>
            <!--begin::Card header-->
            <!--begin::Card body-->
            <div class="card-body p-9">
                <!--begin::Row-->
                <div class="row mb-7">
                    <!--begin::Label-->
                    <table class="table table-row-bordered gy-5" id="table_emon_cleansing_<?=strtolower($format);?>"></table>
                    <!--end::Col-->
                </div>
                <!--end::Row-->
            </div>
            <!--end::Card body-->
        </div>
        <!--end::details View-->
        <!--end::Row-->
    </div>
    <!--end::Container-->
</div>
<!--end::Content-->
<?php
}else{
	echo "<script>window.location.href='".APP_URL."auth'</script>";
}
?>