<?php
if(isset($_SESSION['token'])){
    $code = $_GET['params'];
    $sid->select("list_emon","*",NULL,NULL,NULL,"tabel='$code'");
    $emon = $sid->getResult();
    $tanggal = tanggal($emon[0]['tanggal']);
    $format = $emon[0]['format'];
    $format = $emon[0]['format'];
    if($format == "b"){
        $format = "Baru";
    }else{
        $format = "Lama";
    }
?>
<!--begin::Search form-->
<div class="card rounded-0 bgi-no-repeat bgi-position-x-end bgi-size-cover" style="background-color: #663259;background-size: auto 100%; background-image: url(<?php echo APP_ASSETS;?>media/misc/taieri.svg)">
	<!--begin::body-->
	<div class="card-body container pt-10 pb-8">
		<!--begin::Title-->
		<div class="d-flex align-items-center">
			<h1 class="fw-bold me-3 text-white">Cari</h1>
			<span class="fw-bold text-white opacity-50">Sistem Integrasi Data</span>
		</div>
		<!--end::Title-->
		<!--begin::Wrapper-->
		<div class="d-flex flex-column">
			<!--begin::Block-->
			<div class="d-lg-flex align-lg-items-center">
				<!--begin::Simple form-->
				<div class="rounded d-flex flex-column flex-lg-row align-items-lg-center bg-white p-5 w-xxl-850px h-lg-60px me-lg-10 my-5">
					<!--begin::Row-->
					<div class="row flex-grow-1 mb-5 mb-lg-0">
						<!--begin::Col-->
						<div class="col-lg-12 d-flex align-items-center mb-3 mb-lg-0">
							<!--begin::Svg Icon | path: icons/duotone/General/Search.svg-->
							<span class="svg-icon svg-icon-1 svg-icon-gray-400 me-1">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24" />
										<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
										<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
									</g>
								</svg>
							</span>
							<!--end::Svg Icon-->
							<!--begin::Input-->
							<input type="text" class="form-control form-control-flush flex-grow-1" name="search" value="" placeholder="Pencarian anda" />
							<!--end::Input-->
						</div>
						<!--end::Col-->
						<!--begin::Col-->
						<div class="col-lg-4 d-flex align-items-center mb-5 mb-lg-0 d-none">
							<!--begin::Desktop separartor-->
							<div class="bullet bg-secondary d-none d-lg-block h-30px w-2px me-5"></div>
							<!--end::Desktop separartor-->
							<!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks-2.svg-->
							<span class="svg-icon svg-icon-1 svg-icon-gray-400 me-1">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
										<rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
										<rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
										<rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
									</g>
								</svg>
							</span>
							<!--end::Svg Icon-->
							<!--begin::Select-->
							<select class="form-select border-0 flex-grow-1" data-control="select2" data-placeholder="Category" data-hide-search="true">
								<option value=""></option>
								<option value="1" selected="selected">Category</option>
								<option value="2">In Progress</option>
								<option value="3">Done</option>
							</select>
							<!--end::Select-->
						</div>
						<!--end::Col-->
						<!--begin::Col-->
						<div class="col-lg-4 d-flex align-items-center d-none">
							<!--begin::Desktop separartor-->
							<div class="bullet bg-secondary d-none d-lg-block h-30px w-2px me-5"></div>
							<!--end::Desktop separartor-->
							<!--begin::Svg Icon | path: icons/duotone/Map/Marker1.svg-->
							<span class="svg-icon svg-icon-1 svg-icon-gray-400 me-3">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24" />
										<path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero" />
									</g>
								</svg>
							</span>
							<!--end::Svg Icon-->
							<!--begin::Link-->
							<a href="#" class="btn btn-color-muted px-0 text-start rounded-0 py-1" id="kt_modal_select_location_target" data-bs-toggle="modal" data-bs-target="#kt_modal_select_location">Location</a>
							<!--end::Link-->
						</div>
					</div>
					<!--end::Row-->
					<!--begin::Action-->
					<div class="min-w-150px text-end">
						<button type="submit" class="btn btn-dark" id="kt_advanced_search_button_1">Cari</button>
					</div>
					<!--end::Action-->
				</div>
				<!--end::Simple form-->
				<!--begin::Action-->
				<div class="d-flex align-items-center">
					<a class="fw-bold link-white fs-5" href="pages/search/horizontal.html">Pencarian lebih</a>
				</div>
				<!--end::Action-->
			</div>
			<!--end::Block-->
		</div>
		<!--end::Wrapper-->
	</div>
	<!--end::body-->
</div>
<!--end::Search form-->
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Container-->
    <div class="container" id="kt_content_container">
        <!--begin::Navbar-->
        <div class="card mb-5 mb-xl-10">
            <div class="card-body pt-9 pb-0">
                <!--begin::Details-->
                <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                    <!--begin::Info-->
                    <div class="flex-grow-1">
                        <!--begin::Title-->
                        <div id="content_info"></div>
                        <!--end::Title-->
                    </div>
                    <!--end::Info-->
                </div>
                <!--end::Details-->
                <!--begin::Navs-->
                <!-- <div class="d-flex overflow-auto h-55px">
                    <div id="content_counter"></div>
                </div> -->
                <!--begin::Navs-->
            </div>
        </div>
        <!--end::Navbar-->
        <!--begin::details View-->
        <div class="card mb-5 mb-xl-10">
            <!--begin::Card header-->
            <div class="card-header cursor-pointer">
                <!--begin::Card title-->
                <div class="card-title m-0">
                    <h3 class="fw-bolder m-0">TOTAL JUMLAH PEMAKETAN SELURUH PUPR BERDASARKAN DATA EMON TANGGAL <?=$tanggal;?></h3>
                </div>
                <!--end::Card title-->
            </div>
            <!--begin::Card header-->
            <!--begin::Card body-->
            <div class="card-body p-9">
                <!--begin::Row-->
                <div class="row mb-7">
                    <!--begin::Label-->
					<div class="col-sm-12 col-md-8">
						<div id="grafik_lihat_emon_pbj_paket" class="chartdiv"></div>
					</div>
					<div class="col-sm-12 col-md-4">
						<table class="table table-row-bordered gy-5" id="table_lihat_emon_pbj_paket"></table>
					</div>
                    <!--end::Col-->
                </div>
                <!--end::Row-->
            </div>
            <!--end::Card body-->
        </div>
        <div class="card mb-5 mb-xl-10">
            <!--begin::Card header-->
            <div class="card-header cursor-pointer">
                <!--begin::Card title-->
                <div class="card-title m-0">
                    <h3 class="fw-bolder m-0">TOTAL NILAI KONTRAK PEMAKETAN SELURUH PUPR BERDASARKAN DATA EMON TANGGAL <?=$tanggal;?></h3>
                </div>
                <!--end::Card title-->
            </div>
            <!--begin::Card header-->
            <!--begin::Card body-->
            <div class="card-body p-9">
                <!--begin::Row-->
                <div class="row mb-7">
                    <!--begin::Label-->
					<div class="col-sm-12 col-md-8">
						<div id="grafik_lihat_emon_pbj_nilai" class="chartdiv"></div>
					</div>
					<div class="col-sm-12 col-md-4">
						<table class="table table-row-bordered gy-5" id="table_lihat_emon_pbj_nilai"></table>
					</div>
                    <!--end::Col-->
                </div>
                <!--end::Row-->
            </div>
            <!--end::Card body-->
        </div>
        <!--end::details View-->
        <!--end::Row-->
    </div>
    <!--end::Container-->
</div>
<!--end::Content-->
<script>
am4core.ready(function() {
	am4core.useTheme(am4themes_animated);
	am4core.useTheme(am4themes_material);
	var chart = am4core.create("grafik_lihat_emon_pbj_paket", am4charts.XYChart);
	
	<?
	if(isset($option2)){
	?>
	chart.dataSource.url = "<?= APP_URL;?>chart/emon_pbj_paket.php?table=<?=$code?>&jenis_kontrak=<?=$kode?>&provinsi=<?=$option?>&unor=<?=$option1?>&bu=<?=$option2?>";
	<?
	}else{
	?>
	chart.dataSource.url = "<?= APP_URL;?>chart/emon_pbj_paket.php?table=<?=$code?>";
	<?
	}
	?>
	chart.responsive.enabled = true;
	chart.colors.list = [
		am4core.color("#FF4500"),
		am4core.color("#FFD700"),
		am4core.color("#3399FF"),
		am4core.color("#66FF33"),
		am4core.color("#e60000"),
		am4core.color("#F9F871"),
	];

	var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
	categoryAxis.renderer.grid.template.location = 0;
	// categoryAxis.title.text = "Kategori";
	categoryAxis.dataFields.category = "kategori";
	categoryAxis.renderer.line.strokeOpacity = 1;
	categoryAxis.renderer.minGridDistance = 30;
	categoryAxis.tooltip.disabled = true;
	categoryAxis.renderer.labels.template.rotation = 0;
	categoryAxis.renderer.line.opacity = 0;
	categoryAxis.renderer.cellStartLocation = 0.2;
	categoryAxis.renderer.cellEndLocation = 0.8;

	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
	valueAxis.title.text = "Jumlah Paket";
	valueAxis.cursorTooltipEnabled = false;

	var series1 = chart.series.push(new am4charts.ColumnSeries());
	series1.columns.template.width = am4core.percent(100);
	series1.columns.template.tooltipText = "Tahun {name}: {valueY} Paket";
	series1.tooltipText = "Tahun {name}: {valueY} Paket";
	series1.columns.template.strokeWidth = 0;
	series1.name = "2015";
	series1.dataFields.categoryX = "kategori";
	series1.dataFields.valueY = "lima_belas";
	series1.columns.template.strokeOpacity = 0;
	series1.tooltip.pointerOrientation = "vertical";

	var series2 = chart.series.push(new am4charts.ColumnSeries());
	series2.columns.template.width = am4core.percent(100);
	series2.columns.template.tooltipText = "Tahun {name}: {valueY} Paket";
	series2.tooltipText = "Tahun {name}: {valueY} Paket";
	series2.columns.template.strokeWidth = 0;
	series2.name = "2016";
	series2.dataFields.categoryX = "kategori";
	series2.dataFields.valueY = "enam_belas";
	series2.columns.template.strokeOpacity = 0;
	series2.tooltip.pointerOrientation = "vertical";

	var series3 = chart.series.push(new am4charts.ColumnSeries());
	series3.columns.template.width = am4core.percent(100);
	series3.columns.template.tooltipText = "Tahun {name}: {valueY} Paket";
	series3.tooltipText = "Tahun {name}: {valueY} Paket";
	series3.columns.template.strokeWidth = 0;
	series3.name = "2017";
	series3.dataFields.categoryX = "kategori";
	series3.dataFields.valueY = "tujuh_belas";
	series3.columns.template.strokeOpacity = 0;
	series3.tooltip.pointerOrientation = "vertical";

	var series4 = chart.series.push(new am4charts.ColumnSeries());
	series4.columns.template.width = am4core.percent(100);
	series4.columns.template.tooltipText = "Tahun {name}: {valueY} Paket";
	series4.tooltipText = "Tahun {name}: {valueY} Paket";
	series4.columns.template.strokeWidth = 0;
	series4.name = "2018";
	series4.dataFields.categoryX = "kategori";
	series4.dataFields.valueY = "lapan_belas";
	series4.columns.template.strokeOpacity = 0;
	series4.tooltip.pointerOrientation = "vertical";

	var series5 = chart.series.push(new am4charts.ColumnSeries());
	series5.columns.template.width = am4core.percent(100);
	series5.columns.template.tooltipText = "Tahun {name}: {valueY} Paket";
	series5.tooltipText = "Tahun {name}: {valueY} Paket";
	series5.columns.template.strokeWidth = 0;
	series5.name = "2019";
	series5.dataFields.categoryX = "kategori";
	series5.dataFields.valueY = "bilan_belas";
	series5.columns.template.strokeOpacity = 0;
	series5.tooltip.pointerOrientation = "vertical";
	
	// Add label
	var labelBullet1 = series1.bullets.push(new am4charts.LabelBullet());
	labelBullet1.label.text = "{valueY} Paket";
	labelBullet1.label.fill = am4core.color("#fff");
	labelBullet1.locationY = 0.5;
	labelBullet1.label.rotation = -90;
	labelBullet1.label.hideOversized = false;
	labelBullet1.label.truncate = false;

	var labelBullet2 = series2.bullets.push(new am4charts.LabelBullet());
	labelBullet2.label.text = "{valueY} Paket";
	labelBullet2.label.fill = am4core.color("#fff");
	labelBullet2.locationY = 0.5;
	labelBullet2.label.rotation = -90;
	labelBullet2.label.hideOversized = false;
	labelBullet2.label.truncate = false;

	var labelBullet3 = series3.bullets.push(new am4charts.LabelBullet());
	labelBullet3.label.text = "{valueY} Paket";
	labelBullet3.label.fill = am4core.color("#fff");
	labelBullet3.locationY = 0.5;
	labelBullet3.label.rotation = -90;
	labelBullet3.label.hideOversized = false;
	labelBullet3.label.truncate = false;

	var labelBullet4 = series4.bullets.push(new am4charts.LabelBullet());
	labelBullet4.label.text = "{valueY} Paket";
	labelBullet4.label.fill = am4core.color("#fff");
	labelBullet4.locationY = 0.5;
	labelBullet4.label.rotation = -90;
	labelBullet4.label.hideOversized = false;
	labelBullet4.label.truncate = false;
	
	var labelBullet5 = series5.bullets.push(new am4charts.LabelBullet());
	labelBullet5.label.text = "{valueY} Paket";
	labelBullet5.label.fill = am4core.color("#fff");
	labelBullet5.locationY = 0.5;
	labelBullet5.label.rotation = -90;
	labelBullet5.label.hideOversized = false;
	labelBullet5.label.truncate = false;

	// series.columns.template.adapter.add("fill", function (fill, target) {
	// 	return chart.colors.getIndex(target.dataItem.index);
	// });

	chart.cursor = new am4charts.XYCursor();

	// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set

	chart.legend = new am4charts.Legend();
	chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;

	var marker = chart.legend.markers.template.children.getIndex(0);
	marker.cornerRadius(12, 12, 12, 12);
	marker.strokeWidth = 2;
	marker.strokeOpacity = 1;
	marker.stroke = am4core.color("#ccc");

	chart.exporting.menu = new am4core.ExportMenu();
	chart.exporting.menu.align = "left";
	chart.exporting.menu.verticalAlign = "top";
	chart.dataSource.parser = new am4core.JSONParser();
	chart.dataSource.parser.options.emptyAs = 0;
	chart.scrollbarX = new am4core.Scrollbar();
});

am4core.ready(function() {
	am4core.useTheme(am4themes_animated);
	am4core.useTheme(am4themes_material);
	var chart = am4core.create("grafik_lihat_emon_pbj_nilai", am4charts.XYChart);
	
	<?
	if(isset($kode)){
	?>
	chart.dataSource.url = "<?= APP_URL;?>chart/emon_pbj_nilai.php?table=<?=$code?>&jenis_kontrak=<?=$kode?>&provinsi=<?=$option?>&unor=<?=$option1?>&bu=<?=$option2?>";
	<?
	}else{
	?>
	chart.dataSource.url = "<?= APP_URL;?>chart/emon_pbj_nilai.php?table=<?=$code?>";
	<?
	}
	?>
	
	chart.responsive.enabled = true;
	chart.colors.list = [
		am4core.color("#FF4500"),
		am4core.color("#FFD700"),
		am4core.color("#3399FF"),
		am4core.color("#66FF33"),
		am4core.color("#e60000"),
		am4core.color("#F9F871"),
	];

	var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
	categoryAxis.renderer.grid.template.location = 0;
	// categoryAxis.title.text = "Kategori";
	categoryAxis.dataFields.category = "kategori";
	categoryAxis.renderer.line.strokeOpacity = 1;
	categoryAxis.renderer.minGridDistance = 30;
	categoryAxis.tooltip.disabled = true;
	categoryAxis.renderer.labels.template.rotation = 0;
	categoryAxis.renderer.line.opacity = 0;
	categoryAxis.renderer.cellStartLocation = 0.2;
	categoryAxis.renderer.cellEndLocation = 0.8;

	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
	valueAxis.title.text = "Jumlah Nilai Kontrak";
	valueAxis.cursorTooltipEnabled = false;

	var series1 = chart.series.push(new am4charts.ColumnSeries());
	series1.columns.template.width = am4core.percent(100);
	series1.columns.template.tooltipText = "Tahun {name}: Rp. {valueY.value}";
	series1.tooltipText = "Tahun {name}: Rp. {valueY.value}";
	series1.columns.template.strokeWidth = 0;
	series1.name = "2015";
	series1.dataFields.categoryX = "kategori";
	series1.dataFields.valueY = "lima_belas";
	series1.columns.template.strokeOpacity = 0;
	series1.tooltip.pointerOrientation = "vertical";

	var series2 = chart.series.push(new am4charts.ColumnSeries());
	series2.columns.template.width = am4core.percent(100);
	series2.columns.template.tooltipText = "Tahun {name}: Rp. {valueY.value}";
	series2.tooltipText = "Tahun {name}: Rp. {valueY.value}";
	series2.columns.template.strokeWidth = 0;
	series2.name = "2016";
	series2.dataFields.categoryX = "kategori";
	series2.dataFields.valueY = "enam_belas";
	series2.columns.template.strokeOpacity = 0;
	series2.tooltip.pointerOrientation = "vertical";

	var series3 = chart.series.push(new am4charts.ColumnSeries());
	series3.columns.template.width = am4core.percent(100);
	series3.columns.template.tooltipText = "Tahun {name}: Rp. {valueY.value}";
	series3.tooltipText = "Tahun {name}: Rp. {valueY.value}";
	series3.columns.template.strokeWidth = 0;
	series3.name = "2017";
	series3.dataFields.categoryX = "kategori";
	series3.dataFields.valueY = "tujuh_belas";
	series3.columns.template.strokeOpacity = 0;
	series3.tooltip.pointerOrientation = "vertical";

	var series4 = chart.series.push(new am4charts.ColumnSeries());
	series4.columns.template.width = am4core.percent(100);
	series4.columns.template.tooltipText = "Tahun {name}: Rp. {valueY.value}";
	series4.tooltipText = "Tahun {name}: Rp. {valueY.value}";
	series4.columns.template.strokeWidth = 0;
	series4.name = "2018";
	series4.dataFields.categoryX = "kategori";
	series4.dataFields.valueY = "lapan_belas";
	series4.columns.template.strokeOpacity = 0;
	series4.tooltip.pointerOrientation = "vertical";

	var series5 = chart.series.push(new am4charts.ColumnSeries());
	series5.columns.template.width = am4core.percent(100);
	series5.columns.template.tooltipText = "Tahun {name}: Rp. {valueY.value}";
	series5.tooltipText = "Tahun {name}: Rp. {valueY.value}";
	series5.columns.template.strokeWidth = 0;
	series5.name = "2019";
	series5.dataFields.categoryX = "kategori";
	series5.dataFields.valueY = "bilan_belas";
	series5.columns.template.strokeOpacity = 0;
	series5.tooltip.pointerOrientation = "vertical";
	
	// Add label
	var labelBullet1 = series1.bullets.push(new am4charts.LabelBullet());
	labelBullet1.label.text = "Rp. {valueY}";
	labelBullet1.label.fill = am4core.color("#fff");
	labelBullet1.locationY = 0.5;
	labelBullet1.label.rotation = -90;
	labelBullet1.label.hideOversized = false;
	labelBullet1.label.truncate = false;

	var labelBullet2 = series2.bullets.push(new am4charts.LabelBullet());
	labelBullet2.label.text = "Rp. {valueY}";
	labelBullet2.label.fill = am4core.color("#fff");
	labelBullet2.locationY = 0.5;
	labelBullet2.label.rotation = -90;
	labelBullet2.label.hideOversized = false;
	labelBullet2.label.truncate = false;

	var labelBullet3 = series3.bullets.push(new am4charts.LabelBullet());
	labelBullet3.label.text = "Rp. {valueY}";
	labelBullet3.label.fill = am4core.color("#fff");
	labelBullet3.locationY = 0.5;
	labelBullet3.label.rotation = -90;
	labelBullet3.label.hideOversized = false;
	labelBullet3.label.truncate = false;

	var labelBullet4 = series4.bullets.push(new am4charts.LabelBullet());
	labelBullet4.label.text = "Rp. {valueY}";
	labelBullet4.label.fill = am4core.color("#fff");
	labelBullet4.locationY = 0.5;
	labelBullet4.label.rotation = -90;
	labelBullet4.label.hideOversized = false;
	labelBullet4.label.truncate = false;
	
	var labelBullet5 = series5.bullets.push(new am4charts.LabelBullet());
	labelBullet5.label.text = "Rp. {valueY}";
	labelBullet5.label.fill = am4core.color("#fff");
	labelBullet5.locationY = 0.5;
	labelBullet5.label.rotation = -90;
	labelBullet5.label.hideOversized = false;
	labelBullet5.label.truncate = false;

	// series.columns.template.adapter.add("fill", function (fill, target) {
	// 	return chart.colors.getIndex(target.dataItem.index);
	// });

	chart.cursor = new am4charts.XYCursor();

	// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set

	chart.legend = new am4charts.Legend();
	chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;

	var marker = chart.legend.markers.template.children.getIndex(0);
	marker.cornerRadius(12, 12, 12, 12);
	marker.strokeWidth = 2;
	marker.strokeOpacity = 1;
	marker.stroke = am4core.color("#ccc");

	chart.exporting.menu = new am4core.ExportMenu();
	chart.exporting.menu.align = "left";
	chart.exporting.menu.verticalAlign = "top";
	chart.dataSource.parser = new am4core.JSONParser();
	chart.dataSource.parser.options.emptyAs = 0;
	chart.scrollbarX = new am4core.Scrollbar();
});
</script>
<?php
}else{
	echo "<script>window.location.href='".APP_URL."auth'</script>";
}
?>