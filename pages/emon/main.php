<?php
if(isset($_SESSION['token'])){
    if(isset($_GET['param'])){
        $modul = $_GET['modul'];
        $param = $_GET['param'];
        switch($param){
            case $param:
                $filename = 'pages/'.$modul.'/'.$param.'.php';
                if (!file_exists($filename)) {
                    require_once('pages/cs.php');
                }else{
                    require_once('pages/'.$modul.'/'.$param.'.php');
                }
            break;
        }
    }else{
?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container" id="kt_content_container">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <div class="d-flex align-items-center position-relative d-none my-1"></div>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#MtambahEmon">
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <rect fill="#000000" x="4" y="11" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000)" x="4" y="11" width="16" height="2" rx="1" />
                                </svg>
                            </span>
                            Tambah Emon
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <table class="table table-row-bordered gy-5" id="table_emon"></table>
            </div>
        </div>
        <div class="modal fade" id="MtambahEmon" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered mw-650px">
                <div class="modal-content">
                    <form class="form" id="form_tambah">
                        <div class="modal-header" id="tambah_emon_header">
                            <h2 class="fw-bolder">Tambah emon</h2>
                            <div data-bs-dismiss="modal" class="btn btn-icon btn-sm btn-active-icon-primary">
                                <span class="svg-icon svg-icon-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                            <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                            <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                        </g>
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div class="modal-body py-10 px-lg-17">
                            <div class="scroll-y me-n7 pe-7" id="tambah_emon_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#tambah_emon_header" data-kt-scroll-wrappers="#tambah_emon_scroll" data-kt-scroll-offset="300px">
                                <div class="fv-row mb-7">
                                    <label class="required fs-6 fw-bold mb-2">Nama</label>
                                    <input type="text" id="nama_emon" name="nama" class="form-control form-control-solid" placeholder="Masukkan Nama e-Mon" required/>
                                </div>
                                <div class="fv-row mb-7">
                                    <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                                    <input type="text" id="tanggal_emon" name="tanggal" class="form-control form-control-solid" placeholder="Masukkan Tanggal e-Mon" required readonly />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer flex-center">
                            <button type="button" data-bs-dismiss="modal" class="btn btn-light me-3">Discard</button>
                            <button type="button" onclick="handle_save_modal('tambah_emon','#form_tambah','#MtambahEmon','#table_emon');" id="tambah_emon" class="btn btn-primary">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <form id="form_upload_emon">
            <div class="modal fade" id="ModalUploadEmon" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered mw-650px">
                    <div class="modal-content" id="ContentUploadEmon"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
    }
}else{
	echo "<script>window.location.href='".APP_URL."auth'</script>";
}
?>